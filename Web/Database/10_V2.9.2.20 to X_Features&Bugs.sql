
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.20 Starts(8 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ********************** ZD 100263 Starts ********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	RequestID nvarchar(MAX) NULL
GO
COMMIT

update Usr_List_D set RequestID=''

/* ********************** ZD 100263 End ********************** */


/* ********************** ZD 100164 START********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableAdvancedUserOption smallint NULL
GO
COMMIT


update Org_Settings_D set  EnableAdvancedUserOption = 0

/* ********************** ZD 100164 END ********************** */

/* ********************** ZD 100317 START********************** */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_MailServer_D ADD
	ServerTimeout int NULL
GO

COMMIT

update Sys_MailServer_D set ServerTimeout=10

/* ********************** ZD 100317 END********************** */


/* ********************** ZD 100237 START********************** */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Inv_WorkOrder_D ADD
	woTtlDlvryCost float(53) NULL,
	woTtlSrvceCost float(53) NULL
GO
COMMIT


Update Inv_WorkOrder_D set woTtlDlvryCost =0,woTtlSrvceCost=0


UPDATE  Inv_WorkOrder_D SET
   woTtlDlvryCost = b.totDelCost  + a.deliveryCost,
   woTtlSrvceCost = b.toSerCharge + a.serviceCharge
FROM Inv_WorkOrder_D a , 
(select sum(b.deliveryCost * b.quantity) as totDelCost, SUM(b.serviceCharge * b.quantity) as toSerCharge
, WorkOrderID from Inv_WorkItem_D b group by WorkOrderID) as b
where a.Type = 1 and a.deleted =0 and a.ID = b.WorkOrderID

/* ********************** ZD 100237 End********************** */


/* ********************** ZD 100085 and ZD 100093 Starts********************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
       SetupTimeinMin int NULL,
       TearDownTimeinMin int NULL
GO
COMMIT

/* -- for just reference
select SetupTimeinMin,TearDownTimeinMin,DATEDIFF(minute, confdate, setuptime) as setup,DATEDIFF(minute, setuptime,teardowntime) as duration,
duration - (DATEDIFF(minute, confdate, setuptime)+ DATEDIFF(minute, setuptime,teardowntime)) as tear
,confdate,setuptime,teardowntime,duration,* 
from Conf_Conference_D order by confid desc
*/

-- Update Setup time, Teardown time, duration in minutes from confdate and setuptime

update Conf_Conference_D set SetupTimeinMin = DATEDIFF(minute, confdate, setuptime),
TearDownTimeinMin = duration - (DATEDIFF(minute, confdate, setuptime)+ DATEDIFF(minute, setuptime,teardowntime))
,Duration = DATEDIFF(minute, setuptime,teardowntime)

-- Interchange Confdate and Setuptime

DECLARE @temp AS datetime

UPDATE Conf_Conference_D SET @temp=confdate, confdate=setuptime, setuptime=@temp


--DECLARE @temp AS varchar(10)

--UPDATE Conf_Conference_D SET @temp=confdate, confdate=setuptime, setuptime=@temp,


UPDATE  Conf_Room_D SET
Duration = a.duration + a.SetupTimeinMin + 
a.TearDownTimeinMin + a.McuSetupTime 
FROM Conf_Conference_D a , Conf_Room_D b where a.confid = b.ConfID and b.instanceID = b.instanceID 


/* ********************** ZD 100085 and ZD 100093 End********************** */


--ZD 101689

Update Conf_Conference_D set McuSetupTime = setupTimeinMin
where setupTimeinMin > 0 and confdate > GETUTCDATE() and status in (0,1)

Update Conf_Conference_D set MCUTeardonwnTime = -TeardownTimeinmin 
where TeardownTimeinmin > 0 and confdate > GETUTCDATE() and status in (0,1)

/*********** ZD 100068 Starts  *****************/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	VMRTopTierID int NULL,
	VMRMiddleTierID int NULL
GO
COMMIT
	
Update Org_Settings_D set VMRTopTierID = 0, VMRMiddleTierID = 0


/**NOTE**/

/*******************************************************************************************************
			<<<<<<<<<<<This Query should be runned only once>>>>>>>>>>>>>>>>

********************************************************************************************************/

DECLARE @orgID INT

DECLARE VMRTierCursor CURSOR for
SELECT orgID FROM Org_List_D 

OPEN VMRTierCursor

FETCH NEXT FROM VMRTierCursor INTO @orgID
WHILE @@FETCH_STATUS = 0
BEGIN

Declare @t3vmrid as int, @t2vmrid as int

IF EXISTS (SELECT top 1 Id FROM [Loc_Tier3_D] WHERE Id > 0 and orgId=@orgID)
	Begin
		Select top 1 @t3vmrid = Id FROM [Loc_Tier3_D] WHERE Id > 0 and orgId=@orgID
	End
Else
	Begin
		set @t3vmrid = (select isnull(MAX(Id),0)+ 1 from [Loc_Tier3_D] where orgId=@orgID)
		
		SET IDENTITY_INSERT [Loc_Tier3_D] ON
		
		INSERT INTO [Loc_Tier3_D] ([Id], [Name], [Address], [disabled], [orgId]) 
		VALUES (@t3vmrid, N'Top Tier', N'', 0, @orgID)
		
		SET IDENTITY_INSERT [Loc_Tier3_D] OFF

	End
	
IF EXISTS (SELECT top 1 Id FROM [Loc_Tier2_D] WHERE Id > 0 and orgId=@orgID)
	Begin
		Select top 1 @t2vmrid = Id FROM [Loc_Tier2_D] WHERE Id > 0 and orgId=@orgID
	End
Else
	Begin
		set @t2vmrid = (select isnull(MAX(Id),0)+ 1 from [Loc_Tier2_D] where orgId=@orgID)
		
		SET IDENTITY_INSERT [Loc_Tier2_D] ON

		INSERT INTO [Loc_Tier2_D] ([Id], [Name], [Address], [L3LocationId], [Comment], [disabled], [orgId])
		VALUES (@t2vmrid, N'Middle Tier', NULL, @t3vmrid, N'Tier2-VMR', 0, @orgID)
		
		SET IDENTITY_INSERT [Loc_Tier2_D] OFF
	End


update Org_Settings_D set VMRTopTierID = @t3vmrid, VMRMiddleTierID = @t2vmrid where OrgId = @orgID

FETCH NEXT
FROM VMRTierCursor INTO @orgID
END
CLOSE VMRTierCursor
DEALLOCATE VMRTierCursor

/*********** ZD 100068 End  *****************/

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.25 Ends(26 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.26 Starts(26 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.25 Ends(28 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.26 Starts(28 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.27 Starts(30 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */

/*********** ZD 100510- Hide Unused Languages-Start  *****************/

Delete from Dept_CustomAttr_Option_D where LanguageID in (5,6,7)
Delete from Custom_Lang_D where LanguageId in (5,6,7)
Delete from Gen_Language_S where LanguageID in(2,5,6,7)

/*********** ZD 100510- Hide Unused Languages-End*****************/
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.27 Ends(26 Nov 2013)               */
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.28 Starts							*/
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.28 Ends								*/
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.29 Starts(04 Dec 2013)				*/ 
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.29 Ends(05 Dec 2013)					*/
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.30 Starts							*/
/*                                                                                              */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.30 Ends(12 Dec 2013)					*/
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.31 Starts(13 Dec 2013)				*/
/*                                                                                              */
/* ******************************************************************************************** */

/*********** ZD 100578 - Starts (13 Dec 2013)  *****************/

--SET IDENTITY_INSERT [dbo].[Gen_State_S] ON
--INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (1979,44,N'HK',N'Hong Kong',0)
--SET IDENTITY_INSERT [dbo].[Gen_State_S] OFF
--GO

/*********** ZD 100578- Ends (13 Dec 2013)  *****************/

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.31 Ends(12 Dec 2013)					*/
/*                                                                                              */
/* ******************************************************************************************** */
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.32 Starts							*/
/*                                                                                              */
/* ******************************************************************************************** */


/*********** ZD 100174- Start (19 Dec 2013)  *****************/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	activeSpeaker int NULL
GO
COMMIT

UPDATE Conf_User_D SET activeSpeaker = 0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	activeSpeaker int NULL
GO
COMMIT

UPDATE Conf_Room_D SET activeSpeaker = 0



/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.MCU_Params_D ADD
	activeSpeaker int NULL
GO
COMMIT

UPDATE MCU_Params_D SET activeSpeaker = 0


UPDATE MCU_Params_D SET activeSpeaker = 1 where BridgeTypeid in (4,5,6)


/*********** ZD 100174- End (19 Dec 2013)  *****************/
/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9.2.32 Starts(19 Dec 2013)				*/
/*                                                                                              */
/* ******************************************************************************************** */