/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
menunum = 0;
menus = new Array();
_d = document;
//ZD 102356
var calDefaultView = 2;
var hdnCalDefaultView = document.getElementById("hdnCalDefaultView");
if (hdnCalDefaultView != null)
    calDefaultView = hdnCalDefaultView.value;

function addmenu() {
    menunum++;
    menus[menunum] = menu;
}

function dumpmenus() {
    mt = "<script language=javascript charset='UTF-8'>";
    for (a = 1; a < menus.length; a++) {
        mt += " menu" + a + "=menus[" + a + "];"
    }
    mt += "</script>";
    _d.write(mt)
}
// FB 3055 Ends


if (navigator.appVersion.indexOf("MSIE") > 0) // FB 2815
{
    effect = "Fade(duration=0.2); Alpha(style=0,opacity=88); Shadow(color='#777777', Direction=135, Strength=5)"
}
else {
    effect = "Shadow(color='#777777', Direction=135, Strength=5)"
}

effect = ""			// 2785.2

timegap = 500; followspeed = 5; followrate = 40;
suboffset_top = 10; suboffset_left = 10;

style1 = ["white", "Purple", "Purple", "#FFF7CE", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, , , "66ffff", "000099", "Purple", "white", , "ffffff", "000099"]

style1_2 = ["#041433", "#ffffff", "#046380", "#E0E0E0", "lightblue", 12, "normal", "bold", "Arial, Verdana", 4, "", "", "#66ffff", "#000099", "#041433", "#ffffff", "", "#ffffff", "#000099", ] // FB 2719

style2 = ["black", "#FFF7CE", "#046380", "#FFF7CE", "lightblue", 120, "normal", "bold", "Arial, Verdana", 4, "image/menuarrow.gif", , "#66ffff", "#000099", "#046380", "white", , "ffffff", "000099"]

style2_2 = ["#ffffff", "#C72822", "#C72822", "#ffffff", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2719 FB 2788
style2_3 = ["#ffffff", "#7A0300", "#7A0300", "#ffffff", "transparent", 12, "normal", "bold", "Arial, Verdana", 10, "", "", "66ffff", "000099", "#041433", "#ffffff", "", "#046380", "#000099", ] // FB 2719 FB 2788

var menu1Val, menu5Val;

menu1Val = ["<div id='menuHomeRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Accueil</div>", "SettingSelect2.aspx", , "Go to the Home page", 0]; // FB 2719
if (defaultConfTempJS == "0")//FB 1755
    menu5Val = "ConferenceSetup.aspx?t=n&op=1"; // FB 2719 ZD 101233
else
    menu5Val = "ConferenceSetup.aspx?t=t"; // FB 2719 ZD 101233

if (navigator.appVersion.indexOf("MSIE") > 0) // FB 2815 FB 2827
{

    menu_0 = ["mainmenu", 5, 250, 90, , , style1, 1, "center", effect, , 1, , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]		// 24//FB 1565 FB 2719
	//ZD 101388 Commented
    //if (isExpressUser != null)//FB 1779
    // if (isExpressUser == 1)
    //    menu_0_2 = ["mainmenu", 5, 250, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]

    menu_1 = menu1Val
    menu_2 = ["<div id='menuCalRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Conf&#233;rences</div>", "show-menu=conferences", , "Conferences", 0] // FB 2719
    menu_3 = ["<div id='menuConfRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' onkeypress='fnFireEvent(this)' ><br><br><br><br>Nouvelle<br>conf&#233;rence</div>", "show-menu=confsetup", , "Create a New Conference", 0]; // ZD 101233
    menu_4 = ["<div id='menuSetRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>R&#233;glages</div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    menu_5 = ["<div id='menuAdminRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Administration</div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSiteRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Site</div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCallRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Planifiez un appel</div>", "ExpressConference.aspx?t=n", , "Planifiez un appel", 0] //FB 1779 FB 2827
    //ZD 100167 START
	//menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reservation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_8 = ["<div id='menuReservationRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>R&#233;servation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_9 = ["<div id='menuInstantConferenceRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Appel <br>instantan&#233;e</div>", "javascript:ModalPopUp();", , "Instant Conference", 0]
    //ZD 100167 END
    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
        if (i == 8 && document.getElementById('isCloud').value == '1' && document.getElementById('isExpressUser').value == '1') {
            menu = menu.concat(eval("menu_9"));
        }
    }
    addmenu();

    // ZD 101233 Start
    submenu0_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ]
    submenu0_0_2 = ["conferences", 116, , 128, 1, , style2_2, , "left", effect, , , , , , , , , , , , ]
    //ZD 102356
    if (calDefaultView == 1)
        submenu0_1 = ["Calendrier", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    else
        submenu0_1 = ["Calendrier", "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=", , "Room Calendar", 0]
    submenu0_2 = ["Appeler le Moniteur", "MonitorMCU.aspx", , "Appeler le Moniteur", 0]
    submenu0_3 = ["Tableau de bord", "Dashboard.aspx", , "Tableau de bord", 0] //ZD 102054
    submenu0_4 = ["Liste", "ConferenceList.aspx?t=" + hasConferenceList, , "Liste", 0]

    submenu1_0 = ["confsetup", 116, , 130, 1, , style2, , "left", effect, , , , , , , , , , , , ]
    submenu1_0_2 = ["confsetup", 116, , 130, 1, , style2_2, , "left", effect, , , , , , , , , , , , ]
    submenu1_1 = ["Formulaire express", "ExpressConference.aspx?t=n", , "Formulaire express", 0]
    submenu1_2 = ["Formulaire avanc\u00e8", menu5Val, , "Formulaire avanc\u00e8", 0]
    // ZD 101233 End

    submenu2_0 = ["R&#233;glages", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 145, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Param&#232;tres du compte", "ManageUserProfile.aspx", , "Edit preferences", 0] //ZD101930
    submenu2_2 = ["Rapports", "GraphicalReport.aspx", , "Rapports", 0] //FB 2593 //FB 2885 Start
    submenu2_3 = ["Mod&#232;les", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Groupes", "ManageGroup.aspx", , "manage group", 0]

    submenu3_0 = ["organization", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 145, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Mat&#233;riel", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Mat&#233;riel", "", , , 0]
    submenu3_2 = ["Emplacements", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Emplacements", "", , , 0]
    submenu3_3 = ["Utilisateurs", "show-menu=usr", , "Manage users", 0]
    submenu3_3_ = ["Utilisateurs", "", , , 0]
    submenu3_4 = ["Options", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 = ["R&#233;glages", "OrganisationSettings.aspx", , "Edit Organization Settings", 0] // FB 2719
    submenu3_6 = ["Audiovisuel", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisuel", "", , , 0] // FB 2570
    submenu3_7 = ["Service de restauration", "show-menu=catr", , , 0]
    submenu3_7_ = ["Service de restauration", "", , , 0]
    submenu3_8 = ["Installation", "show-menu=hk", , , 0]
    submenu3_8_ = ["Installation", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["R&#233;glages ", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 190, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 190, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["Points terminaux", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagnostic", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["Les MCU", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["&#201;quilibrage de charge de MCU", "ManageMCUGroups.aspx", , "Équilibrage de charge de MCU", 0] //ZD 100040
    submenu3_1_5 = ["Ponts audio", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_6 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Salles", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Niveau", "ManageTiers.aspx", , "Manage Tiers", 0]
    submenu3_2_3 = ["Planos", "RoomFloor.aspx", , "Floor Plans", 0] //ZD 102123

    submenu3_3_0_2 = ["usr", , , 190, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Utilisateurs actifs", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Gros outil", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["D&#233;partements", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Invit&#233;s", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restaurer l''utilisateur", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Utilisateurs inactifs", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["Importation du r&#233;pertoire LDAP", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["Groupes LDAP", "LDAPGroup.aspx", , "Setup Multiple Groups", 0]
    submenu3_3_8 = ["R&#244;les", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_9 = ["Mod&#232;les", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Inventaires audiovisuels", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Ordres de travail", "ConferenceOrders.aspx?t=1", , "", 0]


    submenu3_7_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 200, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Menus du service de restauration", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Ordres de travail ", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 155, 1, "", style2_3, , "left", effect, , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Services de l''installation", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Ordres de travail  ", "ConferenceOrders.aspx?t=3", , "", 0]

    cur_munu_no = 1; cur_munu_str = "0_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    // ZD 101233 Starts
    cur_munu_no = 2; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    // ZD 101233 Ends

    cur_munu_no = 3; cur_munu_str = "2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 4; cur_munu_str = "3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 5; cur_munu_str = "3_1_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 5) continue; //FB 2023//ZD 100040
            if (EnableEM7Opt == 0 && i == 6) continue; // FB 2633//ZD 100040
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 7; cur_munu_str = "3_3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_6_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_7_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "3_8_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 11; cur_munu_str = "4_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
else {
    menu_0 = ["mainmenu", 5, 250, 110, , , style1, 1, "center", effect, , 1, , , , , , , , , , , ]			// 24 FB 2719
    menu_0_2 = ["mainmenu", 5, 250, 110, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , , ]		// 24 // FB 2050 FB 2719
    menu_1 = menu1Val
    menu_2 = ["<div id='menuCalRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);'><br><br><br><br>Conf&#233;rences</div>", "show-menu=conferences", , "Conferences", 0] // FB 2719
    menu_3 = ["<div id='menuConfRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' onkeypress='fnFireEvent(this)' ><br><br><br><br>Nouvelle<br>conf&#233;rence</div>", "show-menu=confsetup", , "Create a New Conference", 0]; // ZD 101233
    menu_4 = ["<div id='menuSetRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);'><br><br><br><br>R&#233;glages</div>", "show-menu=settings", , "My Settings", 0] // FB 2719
    menu_5 = ["<div id='menuAdminRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Administration</div>", "show-menu=organization", , "Organization", 0] // FB 2719
    menu_6 = ["<div id='menuSiteRed' tabindex='1' onfocus='fnOnFocus(this);' onblur='fnOnBlur(this);' ><br><br><br><br>Site</div>", "show-menu=site", , "Site", 0] // FB 2719
    menu_7 = ["<div id='menuCallRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Planifiez un appel</div>", "ExpressConference.aspx?t=n", , "Planifiez un appel", 0] //FB 1779 FB 2827
    //ZD 100167 START
	//menu_8 = ["<div id='menuReservation' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Reservation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_8 = ["<div id='menuReservationRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>R&#233;servation</div>", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779 FB 2827
    menu_9 = ["<div id='menuInstantConferenceRed' tabindex='1' onkeypress='fnFireEvent(this)' ><br><br><br><br>Appel <br>instantan&#233;e</div>", "javascript:ModalPopUp();", , "Instant Conference", 0]
    //ZD 100167 END
    menu = new Array();
    menu = (if_str == "2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i = 1; i <= mmm_num; i++) {
        menu = (mmm_int & (1 << (mmm_num - i))) ? (menu.concat(eval("menu_" + i))) : menu;
        if (i == 8 && document.getElementById('isCloud').value == '1' && document.getElementById('isExpressUser').value == '1') {
            menu = menu.concat(eval("menu_9"));
        }
    }
    addmenu();

    // ZD 101233 Start
    submenu0_0 = ["conferences", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ]
    submenu0_0_2 = ["conferences", 116, , 128, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ]
    //ZD 102356
    if (calDefaultView == 1)
        submenu0_1 = ["Calendrier", "personalCalendar.aspx?v=1&r=1&hf=&m=&pub=&d=&comp=&f=v", , "Personal Calendar", 0]
    else
        submenu0_1 = ["Calendrier", "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=", , "Room Calendar", 0]
    submenu0_2 = ["Appeler le Moniteur", "MonitorMCU.aspx", , "Appeler le Moniteur", 0]
    submenu0_3 = ["Tableau de bord", "Dashboard.aspx", , "Tableau de bord", 0] //ZD 102054
    submenu0_4 = ["Liste", "ConferenceList.aspx?t=" + hasConferenceList, , "Liste", 0]

    submenu1_0 = ["confsetup", 116, , 130, 1, , style2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_0_2 = ["confsetup", 116, , 130, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ]
    submenu1_1 = ["Formulaire express", "ExpressConference.aspx?t=n", , "Formulaire express", 0]
    submenu1_2 = ["Formulaire avanc\u00e8", menu5Val, , "Formulaire avanc\u00e8", 0]
    // ZD 101233 End

    submenu2_0 = ["R&#233;glages", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_0_2 = ["settings", 116, , 145, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu2_1 = ["Param&#232;tres du compte", "ManageUserProfile.aspx", , "Edit preferences", 0] //ZD 101930
    submenu2_2 = ["Rapports", "GraphicalReport.aspx", , "Rapports", 0] //FB 2593 //FB 2885 Starts
    submenu2_3 = ["Mod&#232;les", "ManageTemplate.aspx", , "Manage Conference Templates", 0]
    submenu2_4 = ["Groupes", "ManageGroup.aspx", , "manage group", 0]

    submenu3_0 = ["organization", 116, , 120, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu3_0_2 = ["organization", 116, , 145, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890

    submenu3_1 = ["Mat&#233;riel", "show-menu=mcu", , "Manage Hardware", 0]
    submenu3_1_ = ["Mat&#233;riel", "", , , 0]
    submenu3_2 = ["Emplacements", "show-menu=loc", , "Manage locations", 0]
    submenu3_2_ = ["Emplacements", "", , , 0]
    submenu3_3 = ["Utilisateurs", "show-menu=usr", , "Manage users", 0]
    submenu3_3_ = ["Utilisateurs", "", , , 0]
    submenu3_4 = ["Options", "mainadministrator.aspx", , "Edit System Settings", 0]
    submenu3_5 = ["R&#233;glages", "OrganisationSettings.aspx", , "Edit Organization Settings", 0]// FB 2719
    submenu3_6 = ["Audiovisuel", "show-menu=av", , , 0]
    submenu3_6_ = ["Audiovisuel", "", , , 0] // FB 2570
    submenu3_7 = ["Service de restauration", "show-menu=catr", , , 0]
    submenu3_7_ = ["Service de restauration", "", , , 0]
    submenu3_8 = ["Installation", "show-menu=hk", , , 0]
    submenu3_8_ = ["Installation", "", , , 0] // FB 2570 // FB 2593 End

    submenu4_0 = ["site", 116, , 100, 1, , style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_0_2 = ["site", 116, , 100, 1, , style2_2, , "left", effect, , , , , , , , , , , , , ] // FB 2719 FB 2857 FB 2890
    submenu4_1 = ["R&#233;glages ", "SuperAdministrator.aspx", , "Edit system settings", 0]

    submenu3_1_0 = ["mcu", , , 190, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_0_2 = ["mcu", , , 190, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_1_1 = ["Points terminaux", "EndpointList.aspx?t=", , "Setup Bridge", 0]
    submenu3_1_2 = ["Diagnostic", "EventLog.aspx", , "View hardware problem log", 0]
    submenu3_1_3 = ["Les MCU", "ManageBridge.aspx", , "Edit Bridge Management", 0]
    submenu3_1_4 = ["&#201;quilibrage de charge de MCU", "ManageMCUGroups.aspx", , "Équilibrage de charge de MCU", 0] //ZD 100040
    submenu3_1_5 = ["Ponts audio", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
    submenu3_1_6 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

    submenu3_2_0 = ["loc", , , 100, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_0_2 = ["loc", , , 100, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_2_1 = ["Salles", "manageroom.aspx?hf=&m=&pub=&d=&comp=&f=&frm=", , "Edit or set up conference room", 0]
    submenu3_2_2 = ["Niveau", "ManageTiers.aspx", , "Manage Tiers", 0]
    submenu3_2_3 = ["Planos", "RoomFloor.aspx", , "Floor Plans", 0] //ZD 102123


    submenu3_3_0 = ["usr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_0_2 = ["usr", , , 190, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_3_1 = ["Utilisateurs actifs", "ManageUser.aspx?t=1", , "Edit, search or create a new user", 0]
    submenu3_3_2 = ["Gros outil", "allocation.aspx", , "Manage Bulk User", 0]
    submenu3_3_3 = ["D&#233;partements", "ManageDepartment.aspx", , "Manage Department", 0]
    submenu3_3_4 = ["Invit&#233;s", "ManageUser.aspx?t=2", , "Edit, search or create a new guest", 0]
    if (sso_int)
        submenu3_3_5 = ["Restaurer l''utilisateur", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    else
        submenu3_3_5 = ["Utilisateurs inactifs", "ManageUser.aspx?t=3", , "Delete or Replaced a removed user", 0]
    submenu3_3_6 = ["Importation du r&#233;pertoire LDAP", "LDAPImport.aspx", , "Setup Multiple Users", 0]
    submenu3_3_7 = ["Groupes LDAP", "LDAPGroup.aspx", , "Setup Multiple Groups", 0]
    submenu3_3_8 = ["R&#244;les", "manageuserroles.aspx", , "Edit, search or create user roles", 0]
    submenu3_3_9 = ["Mod&#232;les", "ManageUserTemplatesList.aspx", , "Edit, search or create user templates", 0]

    submenu3_6_0 = ["av", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_0_2 = ["av", , , 150, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_6_1 = ["Inventaires audiovisuels", "InventoryManagement.aspx?t=1", , "", 0] // FB 2570
    submenu3_6_2 = ["Ordres de travail", "ConferenceOrders.aspx?t=1", , "", 0]


    submenu3_7_0 = ["catr", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_0_2 = ["catr", , , 200, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_7_1 = ["Menus du service de restauration", "InventoryManagement.aspx?t=2", , "", 0] // FB 2570
    submenu3_7_2 = ["Ordres de travail ", "ConferenceOrders.aspx?t=2", , "", 0]

    submenu3_8_0 = ["hk", , , 150, 1, "", style2, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_0_2 = ["hk", , , 155, 1, "", style2_3, , "left", effect, , , , , , , , , , , , , ] // FB 2719
    submenu3_8_1 = ["Services de l''installation", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
    submenu3_8_2 = ["Ordres de travail  ", "ConferenceOrders.aspx?t=3", , "", 0]

    cur_munu_no = 1; cur_munu_str = "0_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableCallMonitor == 0 && i == 2) continue; 
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    // ZD 101233 Starts
    cur_munu_no = 2; cur_munu_str = "1_";
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    // ZD 101233 Ends

    cur_munu_no = 3; cur_munu_str = "2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 4; cur_munu_str = "3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no] - 3; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        for (i = mms_num[cur_munu_no] - 2; i <= mms_num[cur_munu_no]; i++) {
            if (rf_int & 1)
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
            rf_int = rf_int / 2;
        }
        addmenu();
    }

    cur_munu_no = 5; cur_munu_str = "3_1_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if (EnableAudioBridge == 0 && i == 5) continue; //FB 2023//ZD 100040
            if (EnableEM7Opt == 0 && i == 6) continue; // FB 2633//ZD 100040
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 6; cur_munu_str = "3_2_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
    cur_munu_no = 7; cur_munu_str = "3_3_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
                menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 8; cur_munu_str = "3_6_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 9; cur_munu_str = "3_7_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 10; cur_munu_str = "3_8_"; //FB 2593 //FB 2885 // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }

    cur_munu_no = 11; cur_munu_str = "4_"; // ZD 101233
    if (mms_int[cur_munu_no]) {
        menu = new Array();
        menu = menu.concat(eval("submenu" + cur_munu_str + "0" + ((if_str == "2") ? "_2" : "")));
        for (i = 1; i <= mms_num[cur_munu_no]; i++) {
            menu = (mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no] - i))) ? (menu.concat(eval("submenu" + cur_munu_str + i))) : menu;
        }
        addmenu();
    }
}
dumpmenus()

function fnFireEvent(cur) {
    var code = window.event.keyCode;
    if(code == 13)
        cur.parentNode.parentNode.click();
}

function fnOnFocus(cur) {
    //if (cur.id != 'menuAdminRed' && document.getElementById('menuAdminRed') != null)
        //fnOnBlur(document.getElementById('menuAdminRed'));
    cur.parentNode.onmouseover();
}

function fnOnBlur(cur) {
    cur.parentNode.onmouseout();
}

document.onkeydown = function(evt) {
    fnOnKeyDown(evt);
};

function fnOnKeyDown(evt) {
    startTimer(); //ZD 100732
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 40) {
        var str = document.activeElement.id;
        var mainMenu = ["menuCalRed", "menuSetRed", "menuAdminRed", "menuSiteRed"];
        if (str.indexOf("menu") > -1) {
            for (var i = 0; i < mainMenu.length; i++) {
                if (str == mainMenu[i]) {
                    var calSubMenu = [["Calendrier", "Appeler le Moniteur"],
                    ["Param&#232;tres du compte", "Rapports", "Mod&#232;les", "Groupes"], //ZD 101930
                    ["Mat&#233;riel", "Emplacements", "Utilisateurs", "Options", "R&#233;glages", "Audiovisuel", "Service de restauration", "Installation"],
                    ["R&#233;glages "]];
                    for (j = 0; j < calSubMenu[i].length; j++) {
                        var obj = document.getElementById(calSubMenu[i][j]);
                        if (obj != null) {
                            if (i == 2 && j != 3 && j != 4) {
                                obj.parentNode.focus();
                            }
                            else
                                obj.parentNode.parentNode.focus();
                            break;
                        }
                    }
                }
            }
            return false;
        }
        if (document.activeElement.id == "tdtabnav1" || document.activeElement.id == "tdtabnav2") {
            var lst = "accountset,tabnav1,tabnav2,acclogout";
            var nodecount = 1;
            if (navigator.appName.indexOf("Internet Explorer") != -1) {
                nodecount = 0;
            }

            if (lst.indexOf(document.activeElement.childNodes[nodecount].id) > -1)
                tabnavigation();
        }
    }

    if (keyCode == 39) {
        var str = document.activeElement.childNodes[1].id;
        var parentId = document.activeElement.parentNode.id;
        var subMenu = ["Mat&#233;riel", "Emplacements", "Utilisateurs", "Audiovisuel", "Service de restauration", "Installation"]
        if (parentId.indexOf("menu") > -1) {
            for (var i = 0; i < subMenu.length; i++) {
                if (str == subMenu[i]) {
                    var calSubItem = [["Points terminaux", "Diagnostic", "Les MCU", "MCUs Load Balance", "Ponts audio", "EM7"],
                    ["Salles", "Niveau"],
                    ["Utilisateurs actifs", "Gros outil", "D&#233;partements", "Invit&#233;s", "Restaurer l''utilisateur", "Utilisateurs inactifs", "Importation du r&#233;pertoire LDAP", "Groupes LDAP", "R&#244;les", "Mod&#232;les"],
                    ["Inventaires audiovisuels", "Ordres de travail"],
                    ["Menus du service de restauration", "Ordres de travail "],
                    ["Services de l''installation", "Ordres de travail  "]];
                    for (j = 0; j < calSubItem[i].length; j++) {
                        var obj = document.getElementById(calSubItem[i][j]);
                        if (obj != null) {
                            obj.parentNode.parentNode.focus();
                            break;
                        }
                    }
                }
            }
            return false;
        }
    }
    if (keyCode == 37) {
        var lst = "aswitchorgMain";

        if (lst.indexOf(document.activeElement.id) > -1)
            tabnavigationleftarrow();
    }
    if (keyCode == 27) {
        EscClosePopup();
    }
}