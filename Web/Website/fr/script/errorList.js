/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
// max 220

//user info
//var EN_1 = "Veuillez saisir un nom d''identifiant d''utilisateur.";
//var EN_2 = "Veuillez saisir un mot de passe";
//var EN_3 = "Vos saisies pour le mot de passe ne co�ncide pas.";
var EN_4 = "Veuillez saisir un pr\u00E9nom.";
var EN_5 = "Veuillez saisir un nom.";
var EN_6 = "Veuillez saisir une adresse e-mail valide.";
//var EN_51 = "Veuillez saisir un utilisateur dans la liste des utilisateurs.";
var EN_81 = "Veuillez saisir une adresse de soci\u00E9t\u00E9 valide";
var EN_89 = "Veuillez saisir votre nom complet."
//var EN_90 = "Veuillez saisir un objet";
//var EN_91 = "Veuillez saisir un commentaire"
//var EN_129 = "Veuillez saisir un nom de contact";
//var EN_130 = "Veuillez saisir un num\u00E9ro de t\u00E9l\u00E9phone de contact";
//var EN_172 = "Veuillez saisir un e-mail d''un identifiant d''utilisateur.";
//var EN_200 = "Deux adresses e-mail ne co�ncident pas.";


//date and time info
//var EN_7 = "Veuillez v\u00E9rifier la date De puis ressaisissez-la.";
//var EN_8 = "La date De ne peut pas \u00CAtre dans le futur.";
//var EN_9 = "Veuillez saisir une date De ant\u00E9rieure \u00E0 la Date \u00E0.";
//var EN_17 = "Veuillez saisir un nombre dans TempsAvant.";
//var EN_18 = "Veuillez saisir un nombre dans TempsdeDiff\u00E9rence.";
//var EN_69 = "Veuillez saisir une date \u00E0.";
//var EN_70 = "Veuillez saisir une date De.";
//var EN_92 = "La date \u00E0 ne peut pas \u00CAtre dans le futur. ";
//var EN_93 = "Veuillez saisir une date \u00E0 ult\u00E9rieure \u00E0 la date De."
//var EN_117 = "Veuillez saisir une date Jusqu''\u00E0";
//var EN_118 = "La date De ne peut pas \u00CAtre dans le pass\u00E9";
//var EN_119 = "La date Jusqu''\u00E0 ne peut pas \u00CAtre dans le pass\u00E9.";
//var EN_120 = "Cette a d\u00E9j\u00E0 \u00E9t\u00E9 s\u00E9lectionn\u00E9e. Veuillez-en s\u00E9lectionner une autre.";
//var EN_177 = "Veuillez saisir une heure de fin \u00E9tant au moins 15 minutes apr\u00E8s l''heure de D\u00E9but.";
var EN_202 = "Le changement de date pour la personnalisation de l''instance est interdite. Veuillez faire glisser l''instance vers un autre cr\u00E9neau horaire. L''heure ne changera pas.";
//var EN_215 = "Veuillez utiliser le format d''heure correct (hh:mm AM/PM).";
//var EN_216 = "Veuillez comparer l''heure de d\u00E9but de la salle avec l''heure de fin de la conf\u00E9rence.";
//var EN_217 = "Veuillez comparer l''heure de fin de la salle avec l''heure de fin de la conf\u00E9rence.";
//var EN_218 = "Veuillez comparer l''heure de d\u00E9but de la salle avec l''heure de d\u00E9but de la conf\u00E9rence.";
//var EN_220 = "Veuillez comparer l''heure de fin de la salle avec l''heure de d\u00E9but de la conf\u00E9rence.";

// bridge and port info
//var EN_11 = "Veuillez saisir un nom de MCU.";
//var EN_12 = "Veuillez saisir une adresse de MCU.";
//var EN_13 = "Veuillez saisir un identifiant de MCU.";
//var EN_19 = "Veuillez saisir un nombre dans Intervalle de l''enqu\u00CAte.";
//var EN_20 = "Veuillez saisir un nombre (entre 1 et 12) dans Canaux T1 par carte.";
//var EN_21 = "Veuillez saisir un nombre (entre 1 et 12) dans Nombre de ports IP par carte";
//var EN_22 = "Please enter a number (between 1 and 12) in Number of Reserved T1 Channels.";
//var EN_23 = "Please enter a number (between 1 and 12) in Number of Reserved IP Ports.";
//var EN_24 = "Please enter a number (between 1 and 12) in Number of Audio Ports per card.";
//var EN_25 = "Please enter a number (between 1 and 12) in Reserved Number of Audio Ports.";
//var EN_28 = "Please enter a valid IP Address.";
var EN_82 = "Veuillez saisir une adresse IP du SMTP distant ou un nom.";
var EN_83 = "Veuillez saisir un n� de port.";
//var EN_94 = "Please enter an ISDN Phone Number";
//var EN_95 = "Please check the ISDN Phone Number and reenter";
//var EN_96 = "Please check the IP Address and reenter.";
//var EN_98 = "Please enter a valid ISDN Number.";
//var EN_122 = "Please enter a Service Name.";
var EN_123 = "Veuillez saisir une adresse";
//var EN_124 = "Please enter a valid Start Range.";
//var EN_125 = "Please enter a valid End Range.";
//var EN_126 = "Please enter an End Range bigger than Start Range.";
//var EN_133 = "Please enter a Cascade Name.";
//var EN_134 = "Please select a Protocol type.";
//var EN_135 = "Please select a Connection type.";
//var EN_136 = "Please select a Bridge.";
//var EN_137 = "Please enter a valid IP or ISDN Address for bridge.";
//var EN_138 = "Please select a default IP Service.";
//var EN_139 = "Please select a default ISDN Service.";
//var EN_141 = "Please select a private service.";
//var EN_142 = "Please select a public service.";
//var EN_143 = "Please select a Media.";
//var EN_171 = "Please select an Interface type.";
//var EN_181 = "Please enter a Prefix.";
//var EN_182 = "Please enter an Endpoint Name.";
//var EN_185 = "Please enter an Endpoint Address.";
//var EN_189 = "Please fill a number between 0 and 100 in Reserved Port %.";
//var EN_213 = "Please enter a valid IP Address for  for Control Port.";
//var EN_214 = "Please enter a valid IP Address for Port A.";
var EN_314 = "Veuillez saisir une dur\u00E9e valide."; 
//var EN_315 = "Conference duration should be minimum of 15 mins.(Difference between the duration and buffer period)";
var EN_316 = "Veuillez saisir une dur\u00E9e valide pour le commencement de la conf\u00E9rence.";
var EN_317 = "Veuillez saisir une dur\u00E9e valide pour la fin de la conf\u00E9rence.";

//Group info
var EN_10 = "Veuillez saisir un nom de groupe.";
//var EN_39 = "Please select different group(s) for Available Groups and CC Groups."
var EN_53 = "Veuillez double-cliquer sur le nom de groupe.";
//var EN_99 = "Group must have at least one member.";
//var EN_105 = "Please select different group(s) for Default Group and Default CC Group.";
var EN_128 = "Il n'y a pas participants disponible pour \u00e8tre ajout\u00e8 \u00e1 un groupe. Se il vous pla\u00edt s\u00e8lectionner au moins un participant dans le carnet d'adresses avant afin de les ajouter \u00e1 un groupe."; //FB 1914

//recurring
//var EN_32 = "Please check Recurring Start Time and reenter.";
//var EN_33 = "Please check Recurring Duration and reenter.";
//var EN_34 = "Please check Recurring Timezone and reenter.";
//var EN_35 = "Please check the Type of Recurring Pattern and reenter.";
var EN_36 = "Veuillez v\u00E9rifier le type de fin de la plage r\u00E9currente et ressaisissez-le.";
var EN_37 = "Veuillez v\u00E9rifier le mod\u00E8le de plage r\u00E9currente et ressaisissez-le.";
var EN_38 = "Veuillez v\u00E9rifier le type de plage r\u00E9currente et ressaisissez-le.";
var EN_74 = "Veuillez saisir une future date de commencement r\u00E9currente valide.\n(format de fate�: jj/mm/aaaa)";
//var EN_79 = "Number of Participant for Recurring Conference can't over ";
var EN_107 = "Veuillez s\u00E9lectionner un jour de la semaine.";
var EN_108 = "Veuillez saisir une future date de fin valide.\n(format de date�: jj/mm/aaaa)";
var EN_109 = "Veuillez saisir une future date de fin.\n(format de date�: jj/mm/aaaa)";
var EN_193 = "Veuillez s\u00E9lectionner au moins une date.";
var EN_211 = "Vous avez atteint la limite maximale de dates s\u00E9lectionn\u00E9es personnalis\u00E9es.";

//conference and conference room
//var EN_26 = "Please enter a Conference Room Name.";
//var EN_27 = "Please enter a number in Capacity.";
//var EN_30 = "Please enter a Conference Name.";
//var EN_31 = "Please select a Conference Duration of at least 15 minutes.";
var EN_31 = "La conf\u00E9rence doit durer au moins 15 minutes.";
//var EN_40 = "Please enter a number in Number of Extra Participants.";
//var EN_41 = "Please enter a positive number in Number of Extra Participants.";
//var EN_43 = "Please select a Conference Room.";
//var EN_44 = "Please enter a Conference Password.";
var EN_45 = "Veuillez v\u00E9rifier le format de la date de la conf\u00E9rence puis ressaisissez-la.";
var EN_49 = "Veuillez saisir une future date de conf\u00E9rence.";
//var EN_52 = "Please enter all information for the New User \nbefore you Replace the previous User.";
//var EN_54 = "Please double-click the Room Name.";
//var EN_61 = "The following room(s) have no video facility. Continue to set up the video conference?";
//var EN_62 = "During this time period, the following conference(s) are also scheduled:"
//var EN_71 = "Please select room for the External Attendees.";
//var EN_72 = "Please select External Attendees before you select any room.";
//var EN_75 = "Please select room for the External Attendees by clicking Schedule Room(s).";
//var EN_76 = "You must select External Attendees and room(s) \nbefore scheduling a Room-based conference.";
//var EN_87 = "Please select one of the options above.";
//var EN_97 = "Please enter a Conference Room Number.";
//var EN_102 = "Please enter a Conference Date.";
//var EN_103 = "Please check Number of Extra Participants and reenter.";
//var EN_104 = "Please select room for External Attendees.";
//var EN_112 = "Please select your Room before submitting the request.";
//var EN_113 = "Please select a Conference.";
//var EN_114 = "Please select a room from the Selected Room(s) list.";
//var EN_131 = "Please enter a number in Maximum Number of Concurrent Phone Calls.";
var EN_140 = "L''allocation maximale de 512 caract\u00E8res pour la description de la conf\u00E9rence a \u00E9t\u00E9 atteint.";
var EN_144 = "Vous avez choisi une ou plusieurs salles sans participants. Voulez-vous toujours continuer�?"
//var EN_145 = "Please select at least one Protocol.";
//var EN_146 = "Please select at least one Audio Protocol.";
//var EN_147 = "Please select at least one Video Protocol.";
//var EN_148 = "Please select at least one Line Rate.";
//var EN_149 = "Please select a default Equipment.";
//var EN_150 = "Please input Room Phone Number.";
var EN_186 = "Aucun participant choisi pour la salle. \u00CAtes-vous s\u00FBr de vouloir continuer�?";
var EN_187 = "Le mot de passe de la conf\u00E9rence ne devrait \u00CAtre que num\u00E9rique.";//FB 2017
//var EN_190 = "Please select a endpoint to view details.";
var EN_192 = "Veuillez saisir jusqu''\u00E0 10 e-mails pour les e-mails de multiples assistants.";
//var EN_194 = "Please select Assistant in Charge from Address Book.\nTo show Address Book, click edit icon on the right of the text field."; //ZD 100422
//var EN_195 = "Please select a room from the list or provide your own connection information.";
//var EN_203 = "Please select at least one department to associate with room.";
var EN_204 = "Lors de la s\u00E9lection d''un point terminal, veuillez-en s\u00E9lectionner le m\u00E9dia.";
//var EN_205 = "Please select a endpoint, because you select a type of media.";
var EN_208 = "Vous avez atteint la limite maximum de caract\u00E8res permise pour ce champ.";
//var EN_212 = "Advanced room allocation can only be assigned with non-video conference.\n Please either remove advanced room allocation or create non-video conference.";
//var EN_219 = "Room has already been added.";

//Template
//var EN_50 = "Please enter a Template Name.";
//var EN_63 = "A maximum of five templates may be selected.";

//var EN_55 = "Are you sure you want to remove this group?";
//var EN_56 = "Are you sure you want to remove the selected groups?";
//var EN_57 = "Are you sure you want to remove this conference? \nAll the previously invited participants will be notified of the cancellation.";
//var EN_58 = "Are you sure you want to remove the selected conference(s)?";
//var EN_59 = "Are you sure you want to remove this template?";
//var EN_60 = "Are you sure you want to remove the selected templates?";
//var EN_77 = "Please double-click the Template or Conference.";
//var EN_191 = "Please enter a positive integer in Initial Time.";

// Food/Resource
//var EN_156 = "Please input Order Name.";
//var EN_157 = "An order must contain at least one food item. If you choose to continue, this order will be removed.\nAre you sure you want continue?";
var EN_158 = "Veuillez s\u00E9lectionner une image.";
//var EN_159 = "Please select one resource image.";
//var EN_160 = "Please select one resource item.";
//var EN_161 = "Please input Item Name.";
//var EN_162 = "Please input a correct Item Quantity. It should be positive integer.";
//var EN_163 = "If item quantity is 0, this item will be deleted.\nAre you sure you want continue?";
//var EN_164 = "This resource item is already in the list. Please input a different name or select it to edit.";
//var EN_165 = "Please input a correct price. It should be positive number and with up to two decimals.";
//var EN_166 = "All field are empty.\nThis order will not be saved, if it is new order;\n or it will be deleted, if it is old.\n\nAre you sure you want continue?";
//var EN_167 = "You order for this room will be saved.\nWarning: An order must contain at least one resource item. If you choose to continue, This order will be removed.\nAre you sure you want continue?";
//var EN_168 = "Please input Item Name, Or remove this new item.";
//var EN_169 = "Please select a Category.";
//var EN_170 = "Please input Item Unit Price.";
var EN_178 = "Le syst\u00E8me prend uniquement en charge les images au format JPG ou JPEG. Veuillez les convertir au format JPG ou JPEG \u00E0 l''aide d''un outil graphique, par exemple, Microsoft Photo Editor.";
//var EN_179 = "Please do not upload executable file to avoid any virus.";
//var EN_188 = "Please fill a positive integer in Item Quantity.";

// terminal control
//var EN_183 = "Please enter an Endpoint Name.";
//var EN_184 = "Please enter a valid Guest Email.";
//var EN_206 = "Are you sure you want to terminate the selected endpoint?";


//misc
//var EN_64 = "You must enter a new User to replace the previous User.";
//var EN_65 = "Conference successfully saved to your Lotus Notes Calendar.";
//var EN_66 = "Conference successfully saved to your Outlook Calendar.";
//var EN_67 = "You permissioned this user as a General User but have given the user access to the Super Administrator Preference menu. \nSuggestions: 1) Permission this user as a Super Administrator or \n2) Disable his/her Super Administrator Preference menu.";
//var EN_68 = "Sorry, this page has expired. Please click here to login again.";
//var EN_14 = "Please enter a positive or negative number in Position in Chain.";
//var EN_15 = "Card Number is not a positive or negative number, \nor is over the maximum number.";
//var EN_16 = "Please enter a User Home Page Link.";
//var EN_73 = "The URL tag in Conference Description should be in pair (<url></url>). \nPlease correct it and try again.";
//var EN_78 = "Please use the SELECT button to select a Main Conference Room";
//var EN_80 = "Please double-click the Location.";
//var EN_84 = "Please enter a Connection Timeout.";
//var EN_85 = "Please check the To Date and reenter.";
//var EN_86 = "Sorry, no Resource has a Resource ID of ";
var EN_88 = "La fen\u00CAtre d''origine est manquante. Cette fen\u00CAtre sera ferm\u00E9e.\nVeuillez revenir \u00E0 la page d''origine et essayer \u00E0 nouveau.";
//var EN_100 = "Comment cannot be longer than 500 characters.";
var EN_106 = "Veuillez patienter pendant que nous traitons votre demande.";
//var EN_110 = "Please enter New User's Information.";
//var EN_111 = "New User and the Previous User have the same email. Please reenter.";
//var EN_115 = "Please select a value in Video Session.";
//var EN_116 = "Your browser doesn't support printing. Please use menu item to print."
//var EN_127 = "Warning: you have changed the template preference.\nSave these new changes?";
var EN_151 = "Veuillez saisir un nom de cat\u00E9gorie.";
//var EN_152 = "Please enter a File Name.";
//var EN_154 = "Sorry, cannot select Sort By Room if you have chosen None in Rooms.";
//var EN_155 = "Sorry, cannot select Sort By Room if you have chosen None or Any in Rooms.";
var EN_174 = "\u00CAtes-vous s\u00FBr de vouloir supprimer le fichier�?";
//var EN_175 = "Error: failed to upload the file [reason - folder]. Please provide this to administrator.";
var EN_176 = "Erreur�: Certains fichiers sont en double dans la liste des fichiers mise en ligne.";
//var EN_180 = "Please select at least one report to delete.";
//var EN_196 = "Please input a correct MCU ISDN Port Charge. It should be positive number with up to two decimals.";
//var EN_197 = "Please input a correct ISDN Line Cost. It should be positive number with up to two decimals.";
//var EN_198 = "Please input a correct IP Port Charge. It should be positive number with up to two decimals.";
//var EN_199 = "Please input a correct IP Line Cost. It should be positive number with up to two decimals.";
//var EN_201 = "This page has expired due to inactivity.\nPlease login and try again.";
var EN_207 = "D\u00E9sol\u00E9, une erreur s''est produite avec la SIM. L''Internet ou le syst\u00E8me peut en \u00CAtre la source. \nVeuillez essayer \u00E0 nouveau. Si cela continue de se produire, veuillez en informer l''administrateur. \nLa SIM sera arr\u00CAt\u00E9e maintenant."
var EN_209 = "Seuls des caract\u00E8res alphanum\u00E9riques sont autoris\u00E9s dans les noms de fichiers.";
//var EN_210 = "Please enter a unique email ID."

// browser
var EN_132 = "Votre navigateur a le bloqueur de pop-up activ\u00E9. Veuillez le d\u00E9sactiver pour le site de myVRM, car myVRM utilise des pop-ups qui sont n\u00E9cessaires pour entrer vos informations.";


