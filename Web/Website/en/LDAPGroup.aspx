﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>

<%@ Page Language="C#" Inherits="ns_LDAPGroup.LDAPGroup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<style type="text/css">
.boundfield-hidden {
   display: none;
}
</style>
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
    <script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>
    <script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>
    <script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>
</head>
<body>
    <form id="LDAPGroup" runat="server">
    <input type="hidden" id="hdnGroupRoles" runat="server" /> <%--ZD 102182 --%>
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <table width="60%" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td align="center">
                    <br />
                    <center>
                        <h3>
                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, LDAPGroup_LDAPDirectory%>"
                                runat="server"></asp:Literal>
                        </h3>
                    </center>
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display: none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                    </div>
                    <br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="left" class="blackblodtext">
                    <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, SelectTenant%>" runat="server"></asp:Literal>
                    <asp:DropDownList ID="lstOrganization" CssClass="altLong0SelectFormat" runat="server"
                        onchange="javascript:DataLoading(1);" AutoPostBack="true" DataTextField="OrganizationName"
                        DataValueField="OrgID" OnSelectedIndexChanged="ChangeOrgLDAP" Width="30%">
                    </asp:DropDownList>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="3" cellspacing="0" border="0" width="100%" style="border-color: Blue;
                        border-width: 1px; border-style: Solid;">
                        <tr>
                            <td class="tableHeader" align="center">
                                <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, LDAPImport_ServerAddress%>"
                                    runat="server"></asp:Literal>
                            </td>
                            <td class="tableHeader" align="center">
                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, LDAPImport_Domain%>"
                                    runat="server"></asp:Literal>
                            </td>
                            <td class="tableHeader" align="center">
                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, LDAPImport_Login%>"
                                    runat="server"></asp:Literal>
                            </td>
                            <td class="tableHeader" align="center">
                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Password%>" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="tableBody">
                            <td align="center">
                                <asp:TextBox runat="server" ReadOnly="true" ID="txtserveraddress"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:TextBox runat="server" ReadOnly="true" ID="txtDomain"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:TextBox runat="server" ReadOnly="true" ID="txtlogin"></asp:TextBox>
                            </td>
                            <td align="center">
                                <asp:TextBox runat="server" Style="background-image: url('../image/pwdbg.png'); background-repeat: no-repeat"
                                    ReadOnly="true" ID="txtPassword" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border-color: Blue; border-width: 1px; border-style: Solid;">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td align="left" class="blackblodtext" width="60%">
                                <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, LDAPImport_SearchFilter%>"
                                    runat="server"></asp:Literal>
                                <asp:TextBox ID="txtSearchGroups" Style="vertical-align: middle;" runat="server"
                                    Width="290px" Text="" CssClass="altText" />
                            </td>
                            <td align="left"><%--ZD 102182 --%>
                                <button type="submit" id="btnSearch" runat="server" style="vertical-align: middle;"
                                     class="altMedium0BlueButtonFormat"
                                    onserverclick="SearchLDAPGroup">
                                    <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, LDAPImport_btnDfltSrch%>"
                                        runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border-color: Blue; border-width: 1px; border-style: Solid;"><%--ZD 102182 --%>
                    <div style="height: 370px; overflow-x: hidden; overflow-y: scroll">
                        <asp:DataGrid ID="dgGroups" runat="server" AutoGenerateColumns="False" CellPadding="4" 
                            GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="0" ShowFooter="false"
                            Width="100%" Visible="true" Style="border-collapse: separate">
                            <SelectedItemStyle CssClass="tableBody" />
                            <EditItemStyle CssClass="tableBody" />
                            <AlternatingItemStyle CssClass="tableBody" />
                            <ItemStyle CssClass="tableBody" />
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                            <Columns>
                                <asp:BoundColumn DataField="GroupId" ItemStyle-ForeColor="Transparent" ItemStyle-CssClass="boundfield-hidden" HeaderStyle-CssClass="boundfield-hidden"></asp:BoundColumn> <%--ZD 102182 --%>
                                <asp:BoundColumn DataField="GroupName" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader"
                                    ItemStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:WebResources, LDAP_Group %>" ItemStyle-Width="45%"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" ItemStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkSelectRole" runat="server" href="javascript:void(0);" OnClientClick="javascript:return fnInvokePopup(this);"
                                            Text="<%$ Resources:WebResources, Select %>" ForeColor="Green"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, RoleOrTemplate %>" HeaderStyle-HorizontalAlign="Center"
                                    HeaderStyle-Width="40%" ItemStyle-Width="40%" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtRoleName" Width="250px" Enabled="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RoleName") %>'></asp:TextBox>
                                        <asp:TextBox ID="txtRoleid" Style="display: none;" Text='<%# DataBinder.Eval(Container, "DataItem.RoleId") %>'
                                            runat="server"></asp:TextBox>
                                        <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="ImgDeleteRole"
                                            AlternateText="<%$ Resources:WebResources, Delete %>" OnClientClick="javascript:return fnDeleteRole(this.id,'ImgDeleteRole');"
                                            ToolTip="<%$ Resources:WebResources, Delete %>" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <table width="100%">
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblNoRecords" runat="server" Visible="false" CssClass="lblError"
                                        Text="<%$ Resources:WebResources, logList_lblNoRecords %>"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <%--ZD 102182 --%>
                    <asp:Table id="tblPage" visible="false" runat="server">
                            <asp:TableRow id="TableRow1" runat="server">
                                <asp:TableCell id="TableCell1" font-bold="True" font-names="Verdana" font-size="Small" forecolor="Blue" runat="server">
                                <span class="blackblodtext"><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageUser_Pages%>" runat="server"></asp:Literal></span> </asp:TableCell>
                                <asp:TableCell id="TableCell2" runat="server"></asp:TableCell>
                            </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <br />
                    <input name="Go" id="btnGoBack" visible="false" runat="server" type="button" class="altMedium0BlueButtonFormat"
                        onclick="javascript:fnClose();" value=" <%$ Resources:WebResources, GoBack %> " />
                    <button id="btnSubmit" runat="server" style="vertical-align: middle;" onclick="DataLoading(1);"
                        class="altMedium0BlueButtonFormat" onserverclick="btnSubmit_Click">
                        <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
					<%--ZD 102182 --%>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <input id="hdnSessionData" type="hidden" runat="server" />
                            <input type="button" id="btnSessionUpdt" runat="server" onserverclick="UpdateSessionData" style="display:none" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
           
        </table>
    </div>
    <div id="PopupRolesList" align="center" style="position: absolute; overflow: hidden;
        border: 0px; width: 300px; display: none; background-color: White; top: 380px;
        left: 810px;">
        <table align="center" height="90%">
            <tr>
                <td align="center" class="blackblodtext">
                    <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, UserRolesandTemplates%>" runat="server"></asp:Literal>
                </td>
                <td align="right">
                    <img id="imgClose" src="../image/close-small.gif" alt="<%$ Resources:WebResources, Close%>" runat="server" onclick="javascript:fnDivClose();" />
                    &nbsp;&nbsp;
                </td>
            </tr>
            <tr align="center">
                <td align="center" colspan="2">
                    <asp:ListBox ID="lstUserRoleList" runat="server" Height="225px" Width="300px" SelectionMode="Single"
                        onchange="javascript:return fnRolesSubmit();"></asp:ListBox>
                </td>
            </tr>
        </table>
    </div>
    <input id="invokElem" type="hidden" runat="server" />
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<script type="text/javascript" src="inc/softedge.js"></script>
<script type="text/javascript">
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block';
        else
            document.getElementById("dataLoadingDIV").style.display = 'none';
    }

    function fnClose() {
        window.location.replace("SuperAdministrator.aspx");
        return true;
    }


    function fnInvokePopup(obj) {
        if (document.getElementById("errLabel") != null) document.getElementById("errLabel").value = "0";
        if (document.getElementById("lstUserRoleList") != null) document.getElementById("lstUserRoleList").value = "0";

        var i = 1;
        if (navigator.userAgent.indexOf('Trident') > -1)
            i = 0;

        document.getElementById("invokElem").value = obj.parentNode.childNodes[i].id;

        $('#PopupRolesList').css('left', xx);      // <<< use pageX and pageY
        $('#PopupRolesList').css('top', yy + 10);
        $('#PopupRolesList').css('display', 'inline');
        $("#PopupRolesList").css("position", "absolute");

        $('#PopupRolesList').show();
        return false;
    }

    function fnRolesSubmit() {
        if (document.getElementById("lstUserRoleList").selectedIndex == -1) // ZD 102368
            return false;
        var lstOptions = document.getElementById("<%=lstUserRoleList.ClientID%>");
        var hdnGroupRoles = document.getElementById("hdnGroupRoles"); //ZD 102182
        for (var i = 0; i < lstOptions.options.length; i++) {
            if (lstOptions.options[i].value == document.getElementById("lstUserRoleList").value) {
                var curId = document.getElementById("invokElem").value;
                document.getElementById(curId.replace("lnkSelectRole", "txtRoleName")).value = lstOptions.options[i].text;
                document.getElementById(curId.replace("lnkSelectRole", "txtRoleid")).value = lstOptions.options[i].value;

                //ZD 102182
                var id = curId.split('_')[1].replace("ctl0", "").replace("ctl", "");
                var parent = document.getElementById("<%=dgGroups.ClientID%>");
                var tr = parent.getElementsByTagName("TR")[id-1];
                var tc = tr.getElementsByTagName("TD")[0];
                var tcName = tr.getElementsByTagName("TD")[1];
                
                if (hdnGroupRoles.value.indexOf("|" + tcName.innerHTML + "|") > 0) {
                    var aryGroupRole = hdnGroupRoles.value.split('~');
                    for (var a = 0; a < aryGroupRole.length; a++) {
                        if (tcName.innerHTML == aryGroupRole[a].split('|')[1]) {
                            if (aryGroupRole[a].split('|')[4] == "U")
                                hdnGroupRoles.value = hdnGroupRoles.value.replace(aryGroupRole[a].split('|')[1] + "|" +aryGroupRole[a].split('|')[2] + "|" + aryGroupRole[a].split('|')[3]
                                , aryGroupRole[a].split('|')[1] + "|" + lstOptions.options[i].value + "|" + lstOptions.options[i].text);
                            else
                                hdnGroupRoles.value = hdnGroupRoles.value.replace(aryGroupRole[a].split('|')[1] + "|" + aryGroupRole[a].split('|')[2] + "|" + aryGroupRole[a].split('|')[3] + "|D"
                                , aryGroupRole[a].split('|')[1] + "|" + lstOptions.options[i].value + "|" + lstOptions.options[i].text + "|U");
                        }
                    }
                }
                else {
                    if (hdnGroupRoles.value == "")
                        hdnGroupRoles.value = tc.innerHTML + "|" + tcName.innerHTML + "|" + lstOptions.options[i].value + "|" + lstOptions.options[i].text + "|U";
                    else
                        hdnGroupRoles.value += "~" + tc.innerHTML + "|" + tcName.innerHTML + "|" + lstOptions.options[i].value + "|" + lstOptions.options[i].text + "|U";
                }
                
                document.getElementById("btnSessionUpdt").click();
                break;
            }
        }
        $('#PopupRolesList').hide();
        return false;
    }

    function fnDeleteRole(obj, ctrlName) {
        var delIcon = obj;
        var txtrolname = delIcon.replace(ctrlName, "txtRoleName");
        var txtrolid = delIcon.replace(ctrlName, "txtRoleid");
		//ZD 102182
        var roleID = document.getElementById(txtrolid).value;
        var roleName = document.getElementById(txtrolname).value;

        if (txtrolname != null && document.getElementById(txtrolname) != null)
            document.getElementById(txtrolname).value = "";

        if (txtrolid != null && document.getElementById(txtrolid) != null)
            document.getElementById(txtrolid).value = "";

        //ZD 102182
        var hdnGroupRoles = document.getElementById("hdnGroupRoles");
        var id = delIcon.split('_')[1].replace("ctl0", "").replace("ctl", "");
        var parent = document.getElementById("<%=dgGroups.ClientID%>");
        var tr = parent.getElementsByTagName("TR")[id - 1];
        var tc = tr.getElementsByTagName("TD")[0];
        var tcName = tr.getElementsByTagName("TD")[1];
        
        if (hdnGroupRoles.value.indexOf("|" + tcName.innerHTML +"|") > 0) {
            var aryGroupRole = hdnGroupRoles.value.split('~');
            for (var i = 0; i < aryGroupRole.length; i++) {
                if (tcName.innerHTML == aryGroupRole[i].split('|')[1]) {
                    if (aryGroupRole[i].split('|')[4] == "U")
                        hdnGroupRoles.value = hdnGroupRoles.value.replace(aryGroupRole[i].split('|')[1] + "|" + aryGroupRole[i].split('|')[2] + "|" + aryGroupRole[i].split('|')[3] + "|U"
                        , aryGroupRole[i].split('|')[1] + "|" + roleID + "|" + roleName + "|D");
                    else
                        hdnGroupRoles.value = hdnGroupRoles.value.replace(aryGroupRole[i].split('|')[1] + "|" + aryGroupRole[i].split('|')[2] + "|" + aryGroupRole[i].split('|')[3] + "|D"
                        , aryGroupRole[i].split('|')[1] + "|" + roleID + "|" + roleName + "|D");
                }
            }
        }
        else {            
            if(hdnGroupRoles.value == "")
                hdnGroupRoles.value = tc.innerHTML + "|" + tcName.innerHTML + "|" + roleID + "|" + roleName + "|D";
            else
                hdnGroupRoles.value += "~" + tc.innerHTML + "|" + tcName.innerHTML + "|" + roleID + "|" + roleName + "|D";
        }
        
        document.getElementById("btnSessionUpdt").click();

        return false;
    }

    function fnDivClose() {
        $('#PopupRolesList').hide();
    }

    function EscClosePopup() {
        if (document.getElementById("imgClose") != null) {
            document.getElementById("imgClose").click();
        }
    }


    //    document.getElementById("txtSearchGroups").setAttribute("onfocus", "fnInitSearchFilter();");
    //    document.getElementById("txtSearchGroups").setAttribute("onblur", "fnFireSearchFilter('0');");

    var lastVal, curVal, timer;
    function fnInitSearchFilter() {
        lastVal = document.getElementById("txtSearchGroups").value;
        clearTimeout(timer);
        timer = setTimeout("fnFireSearchFilter('1')", 1000);

    }

    function fnFireSearchFilter() {
        /*if (par == '1') {
        curVal = document.getElementById("txtSearchGroups").value;
        if (lastVal != curVal) {
        fnInitSearchFilter();
        return false;
        }
        fnInitSearchFilter();
        }
        else
        clearTimeout(timer);*/
        var isExist = false;
        curVal = document.getElementById("txtSearchGroups").value;

        if (curVal == "") {
            isExist = true;
            document.getElementById("lblNoRecords").style.display = "none";
        }
         
        var tempVal = curVal.toLowerCase().replace(/^\s+|\s+$/gm, '') + ",";

        var finder = tempVal.split(',');

        var count = document.getElementById('dgGroups').rows.length;

        var ind = 1;
        if (navigator.userAgent.indexOf('Trident') > -1)
            ind = 0;

        for (var i = 1; i < count; i++) {
            document.getElementById("dgGroups").childNodes[ind].childNodes[i].style.display = "table-row";
        }

        var content, found = false;
        if (tempVal != ",") {
            for (var i = 1; i < count; i++) {
                content = document.getElementById("dgGroups").childNodes[ind].childNodes[i].childNodes[ind].innerText.toLowerCase();
                found = false;

                for (var j = 0; j < finder.length; j++) {
                    if (finder[j] != "" && content.indexOf(finder[j]) > -1) {
                        found = true;
                        isExist = true;
                        break;
                    }
                }

                if (found == false)
                    document.getElementById("dgGroups").childNodes[ind].childNodes[i].style.display = "none";
            }
        }

        if (isExist)
            document.getElementById("lblNoRecords").style.display = "none";
        else
            document.getElementById("lblNoRecords").style.display = "";
        
        return false;
    }

    document.onmousemove = getMouseXY;

    var xx, yy;
    function getMouseXY(m) {
        var tmpX, tmpY, iL, iV;

        if (navigator.userAgent.indexOf('Trident') > -1) {
            tmpX = event.clientX;
            tmpY = event.clientY;
        }
        else {
            tmpX = m.pageX;
            tmpY = m.pageY;
        }

        if (!document.body.scrollTop) {
            iL = document.documentElement.scrollLeft;
            iV = document.documentElement.scrollTop;
        }
        else {
            iL = document.body.scrollLeft;
            iV = document.body.scrollTop;
        }

        if (navigator.userAgent.indexOf('Chrome') > -1) {
            xx = tmpX;
            yy = tmpY;
        }
        else {
            xx = tmpX + iL;
            yy = tmpY + iV;
        }
    }

</script>
