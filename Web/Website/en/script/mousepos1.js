/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
var mousedownX = 0;
var mousedownY = 0;

// If it is not IE, we assume that the browser is NS.
var IE = document.all ? true : false
// NS
if (!IE) document.captureEvents(Event.MOUSEMOVE);
// Set-up to use getMouseXY function onMouseMove
document.onMouseMove  = getMouseXY;


function getMouseXY(e) 
{
	// Set-up to use getMouseXY function onMouseMove
	var IE = document.all ? true : false;
	//ZD 100933
    if (IE) { // IE 
        mousedownX = event.clientX + document.body.scrollLeft;
        mousedownY = event.clientY + document.body.scrollTop;
    }
    else if (navigator.userAgent.indexOf('Chrome') > -1) {
        mousedownY = event.pageY;
        mousedownX = event.pageX;
    }
    else {
        mousedownY = window.innerHeight * (25 / 100);
        mousedownX = window.innerWidth * (60 / 100); //ZD 100933
    }  
	// catch possible negative values in NS4
	mousedownX = ( (mousedownX < 0) ? 0 : mousedownX );
	mousedownY = ( (mousedownY < 0) ? 0 : mousedownY );

	return true
}