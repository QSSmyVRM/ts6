﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_EmailList2Popup.en_emaillist2"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>emaillist2</title>
    <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>   
<link rel="stylesheet" type="text/css" href="css/fixedHeader.css"/>
<script type="text/javascript" src="extract.js"></script>
<script type="text/javascript" src="sorttable.js"></script>
<script type="text/javascript" src="inc/functions.js"></script> <%-- ZD 102723 --%>
<script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>

<script type="text/javascript" language="javascript">          
<!--  

//FB 1888
function replaceSpcChar(strName,symbol,srcSym)
{
   do
    {
      strName = strName.replace(srcSym, symbol);
    }while((strName.indexOf(srcSym)) >= "0");
    
    return strName;
}

if (parent.opener && parent.opener.closed) {
	alert(EN_88);
} else {
    var isImFu, isCreate, isFu, isRm, isFuCreate, isRmCreate, isVMR, isVD, isAD; //FB 2376
	if (parent.opener.document.frmSettings2) {
		isImFu = ( ( (parent.opener.document.frmSettings2.CreateBy.value == "1") || (parent.opener.document.frmSettings2.CreateBy.value == "3") || (parent.opener.document.frmSettings2.CreateBy.value == "-1") ) ? true : false ) ;

		isCreate = ( (parent.opener.document.frmSettings2.ConfID.value == "new") ? true : false ) ;
		isFu = ( (parent.opener.document.frmSettings2.CreateBy.value == "1") ? true : false ) ;
		isRm = ( ((parent.opener.document.frmSettings2.CreateBy.value == "2") || (parent.opener.document.frmSettings2.CreateBy.value == "7")) ? true : false ) ;
		if(parent.opener.document.getElementById("isVMR") != null)
            isVMR = ( (parent.opener.document.frmSettings2.isVMR.value == "1") ? true : false ) ;//FB 2376
		isFuCreate = (isFu && isCreate)
		isRmCreate = (isRm && isCreate)
	}

	if ((parent.opener.document.location.href.indexOf(".aspx") > 0) && (parent.opener.document.location.href.indexOf("Orders.aspx") < 0) && (parent.opener.document.location.href.indexOf("BridgeDetails.aspx") < 0) && (parent.opener.document.location.href.indexOf("Administrator.aspx") < 0))
	{
	 if(parent.opener.document.getElementById("CreateBy") != null) //Corrected for ASPX conversion
	 {//Corrected for ASPX conversion
	        isRm = ( (parent.opener.document.getElementById("CreateBy").value == "7") ? true : false ) ;
            isVD = ( ((parent.opener.document.getElementById("CreateBy").value == "2") || (parent.opener.document.getElementById("CreateBy").value == "4")) ? true : false ) ; //FB 1759
            isAD = ( (parent.opener.document.getElementById("CreateBy").value == "6") ? true : false ) ;
            if(parent.opener.document.getElementById("isVMR") != null)
                isVMR = ( (parent.opener.document.getElementById("isVMR").value == "1") ? true : false ) ;//FB 2376
            isFuCreate = (isAD || isVD); //true;
            isImFu = (isAD || isVD || isRm); // && isCreate);;
     }//Corrected for ASPX conversion
     else if (parent.opener.document.frmSettings2) 
     {
            isRm = ( (parent.opener.document.frmSettings2.CreateBy.value == "7") ? true : false ) ;
            isVD = ( ((parent.opener.document.frmSettings2.CreateBy.value == "2") || (parent.opener.document.frmSettings2.CreateBy.value == "4")) ? true : false ) ; //FB 1759
            isAD = ( (parent.opener.document.frmSettings2.CreateBy.value == "6") ? true : false ) ;
            if(parent.opener.document.getElementById("isVMR") != null)
                isVMR = ( (parent.opener.document.frmSettings2.isVMR.value == "1") ? true : false ) ;//FB 2376
            isFuCreate = (isAD || isVD); //true;
            isImFu = (isAD || isVD || isRm); // && isCreate);;
     }
	}

}
function typeimg(ifrmname, sb)
{
	imgsrc = document.getElementById("img_" + ifrmname + "_" + sb).src;
	
	sorting = (imgsrc.indexOf(".gif") == -1) ? 0 : ((imgsrc.indexOf("up.gif")==-1) ? -1 : 1);
	for (i = 1; i < 5; i++) {
		document.getElementById("img_" + ifrmname + "_" + i).src = "image/transparent2.gif";
	}
	document.getElementById("img_" + ifrmname + "_" + sb).src = ((sorting == 0) ? "image/sort_up.gif" : ((sorting == 1) ? "image/sort_up.gif" : "image/sort_up.gif")) ;
}

//function partyChg (pid, pfn, pln, pemail, ischk)
function partyChg (pinfo)
{
    var vmrParty = "0", partysinfo;
    pary = pinfo.split("~"); //FB 1888
	pid = pary[0]; pfn = pary[1]; pln = pary[2]; pemail = pary[3]; ischk = pary[4];psur = pary[5]; // ischk: click "selected" checkbox//FB 2348
	pno = ((queryField("srch") == "y")?5:2) + parseInt(pary[5], 10) * ((isFuCreate || isRm) ? 6 : 7);
	pfn = replaceSpcChar(pfn.replace(new RegExp("!!", "g"), "'"),'\"',"||" ); //FB 1888
	pln = replaceSpcChar(pln.replace(new RegExp("!!", "g"), "'"),'\"',"||" ); //FB 1888
	//ZD 100369_MCU Start
	var duplicate = "0";
	if (parent.opener.document.getElementById("txtMultipleAssistant") != null ) 
	{
	    if(parent.opener.document.getElementById("txtMultipleAssistant").value != "")
	    {
	        var email = parent.opener.document.getElementById("txtMultipleAssistant").value.split(";");
	        for(var i = 0; i < email.length-1 ; i++)
	        {
	            if (typeof String.prototype.trim !== 'function') {
	                String.prototype.trim = function () {
	                    return this.replace(/^\s+|\s+$/g, '');
	                }
	            }
	            
	            if(email[i].trim() == pemail)
	                duplicate = "1";
	        }
	        if(duplicate == "0")
	            parent.opener.document.getElementById("txtMultipleAssistant").value += pemail + ' ; '; 
	    }
	    else
	    {
	        parent.opener.document.getElementById("txtMultipleAssistant").value += pemail + ' ; ';
	    }
	}
	//ZD 100369_MCU End
	//ZD 100167 Start
	duplicate = "0";
	if (parent.opener.document.getElementById("txtInstantConferenceParticipant") != null ) 
	{
	    if(parent.opener.document.getElementById("txtInstantConferenceParticipant").value != "")
	    {
	        var email = parent.opener.document.getElementById("txtInstantConferenceParticipant").value.split(";");
	        for (var i = 0; i < email.length - 1; i++)
             {

                 if (typeof String.prototype.trim !== 'function') {
                     String.prototype.trim = function () {
                         return this.replace(/^\s+|\s+$/g, '');
                     }
                 }

	            if(email[i].trim() == pemail)
	                duplicate = "1";
	        }
	        if(duplicate == "0")
	            parent.opener.document.getElementById("txtInstantConferenceParticipant").value += pemail + ' ; '; 
	    }
	    else
	    {
	        parent.opener.document.getElementById("txtInstantConferenceParticipant").value += pemail + ' ; ';
	    }
	}
	//ZD 100167 End
	switch (queryField("frm")) {
		case "party2":	
			partysinfo = parent.opener.document.frmSettings2.PartysInfo.value;
			cb1 = eval("document.frmEmaillist2.s" + pid);
			break;
		case "party2NET":
		    if (parent.opener.document.getElementById("txtPartysInfo") != null)
			    partysinfo = parent.opener.document.getElementById("txtPartysInfo").value;
			    cb1 = eval("document.frmEmaillist2.s" + pid);
			    break;
		//Code Added by Offshore FB issue no:412-Start (For ManageTemplate2)
	    case "party2Aspx":
			partysinfo = parent.opener.document.getElementById("txtPartysInfo").value;
			cb1 = eval("document.frmEmaillist2.s" + pid);
			break;
	    //Code Added by Offshore FB issue no:412-End
		default:
			partysinfo = parent.opener.document.frmManagegroup2.PartysInfo.value;
			cb1 = document.getElementById("0" + pemail);
			break;
	}
	if (cb1.checked) {
	    var txtTemp;
	    //txtTemp = ",0,1,0,1,0,0,";//FB 1759 -Commented	  
        txtTemp = "!!0!!1!!0!!1!!1!!0!!";//FB 1759//FB 1888
        //txtTemp = ",0,1,0,1,1,0,";//FB 1759
	if ((parent.opener.document.location.href.indexOf(".aspx") > 0) && (parent.opener.document.location.href.indexOf("Orders.aspx") < 0))
    {
        if (isRm)
            txtTemp = "!!0!!1!!0!!1!!0!!0!!"; //FB 1759//FB 1888
            //txtTemp = ",0,1,0,1,0,0,"; //FB 1759
        if (isVD)
            txtTemp = "!!0!!1!!0!!1!!1!!0!!";//FB 1888
            //txtTemp = ",0,1,0,1,1,0,";
        if (isAD)
           //txtTemp = ",0,1,0,1,1,0,"; //FB 1759 -Commented
            txtTemp = "!!0!!1!!0!!1!!0!!1!!";  //FB 1759//FB 1888
            //txtTemp = ",0,1,0,1,0,1,";  //FB 1759
        if (isVMR)
        {
            txtTemp = "!!0!!0!!0!!1!!1!!0!!";//FB 2376 
            vmrParty = "1";
        }
        
		}
		mp = pid + "!!" + pfn + "!!" + pln + "!!" + pemail + txtTemp;//FB 1888
		//mp = pid + "," + pfn + "," + pln + "," + pemail + txtTemp;

		mp += "!!!!!!-5!!!!!!";//FB 1888 //FB 2348
		//mp += ",,,-5,,,;";
		//FB 2348 Start
		if(psur == "1")
		{
		    mp += "1!!"; //default survey to 1
		}
		else
		{
		    mp += "0!!"; //default survey to 0
		}
		//FB 2348 End
		mp += "0!!" + vmrParty + "!!0||"; //FB 2347//FB 2376 //FB 2550 - Public Party
		
      if (pemail == "") 
      {        
        alert("Users without email addresses cannot be added as participants.");
        cb1.checked = false;
      }
      else
          if ((partysinfo != null) && (partysinfo.indexOf("!!" + pemail + "!!") == -1)) {//FB 1888
			partysinfo += mp
		} else {
		    if (partysinfo != null) {
		        partysinfo = replaceSpcChar(partysinfo.replace(new RegExp("!!", "g"), "!"), "?", "||");
		        lp = partysinfo.lastIndexOf("?", partysinfo.indexOf("!" + pemail + "!")) + 1; //FB 1888
		        rp = partysinfo.indexOf("?", partysinfo.indexOf("!" + pemail + "!")) + 1;
		        mp = replaceSpcChar(mp.replace(new RegExp("!!", "g"), "!"), "?", "||");

		        newpartysinfo = partysinfo.substring(0, (lp == "-1") ? 0 : lp);
		        newpartysinfo += mp;
		        newpartysinfo += partysinfo.substring(rp, partysinfo.length);
		        partysinfo = newpartysinfo;
		        partysinfo = replaceSpcChar(partysinfo.replace(new RegExp("!", "g"), "!!"), "||", "?");
		    }
		}
	} else {
		if (ischk=="1") {
			if (partysinfo != null && (partysinfo.indexOf("!!" + pemail + "!!") != -1)) {	//FB 1888
		        partysinfo = replaceSpcChar(partysinfo.replace(new RegExp("!!","g"),"!"),"?","||");
				lp = partysinfo.lastIndexOf("?",partysinfo.indexOf("!" + pemail + "!")) + 1; //FB 1888
				rp = partysinfo.indexOf("?",partysinfo.indexOf("!" + pemail + "!")) + 1;
				newpartysinfo = partysinfo.substring(0, (lp=="-1") ? 0 : lp);
				newpartysinfo += partysinfo.substring(rp, partysinfo.length);
				partysinfo = newpartysinfo;				
		        partysinfo = replaceSpcChar(partysinfo.replace(new RegExp("!","g"),"!!"),"||","?");
			}
		}
	}
	switch (queryField("frm")) {
	    case "party2NET":
	        if (parent.opener.document.getElementById("txtPartysInfo") != null) {
	            parent.opener.document.getElementById("txtPartysInfo").value = partysinfo;
	            parent.opener.ifrmPartylist.location.reload();
	            //parent.opener.ifrmPartylist.bfrRefresh(); //ZD 100834 //ZD 101268
	            if (typeof parent.opener.ifrmPartylist.bfrRefresh == "function") {
	                parent.opener.ifrmPartylist.bfrRefresh();
	            } 
	        }
	        break;
		case "party2":	
			parent.opener.document.frmSettings2.PartysInfo.value = partysinfo;
			parent.opener.ifrmPartylist.location.reload(); 
			break;
		//Code Added by Offshore FB issue no:412-Start(For ManageTemplate2)
		case "party2Aspx":	
			parent.opener.document.frmSettings2.txtPartysInfo.value = partysinfo;
			parent.opener.ifrmPartylist.location.reload(); 
			break;
		//Code Added by Offshore FB issue no:412-End
		default:
			parent.opener.document.frmManagegroup2.PartysInfo.value = partysinfo;
			parent.opener.ifrmMemberlist.location.reload(); 
			break;
	}
}


// managegoup2
function memberChg (pinfo, pemail)
{
	if ( (!parent.opener.document.frmManagegroup2) && (!parent.opener.location.href.indexOf("reportinputparameters.asp"))) {
		alert(EN_106);
		ary = (document.frmEmaillist2.ptyinfos.value).split("||"); // FB 1888
		start_no = 2;
		if (queryField("srch") == "y")
			start_no += 3; 
		start_no -= 1;
		
		found = false;
		for (k = 0; k < ary.length-1; k++) {
			start_no += 1;
			if ( document.frmEmaillist2.elements[start_no].name == ("0" + pemail) ) {
				found = true;
				break;
			}
		}
		if (found) {		
			cb1 = document.frmEmaillist2.elements[start_no];			
			if (cb1.checked)
				cb1.checked = false;
		}


		return false;
	}
	
	gu = parent.opener.document.getElementById("PartysInfo").value
	
	if (document.getElementById("0" + pemail).checked) {
		if ( gu.indexOf("!!" + pemail + "!!") == -1) {//FB 1888
			parent.opener.document.getElementById("PartysInfo").value += pinfo;
		}
	} else {
		if ( gu.indexOf("!!" + pemail + "!!") != -1) {//FB 1888
			lp = gu.lastIndexOf( "||", gu.indexOf("!!" + pemail + "!!") ) + 1;
			rp = gu.indexOf( "||", gu.indexOf("!!" + pemail + "!!") ) + 1;
			newgu = gu.substring(0, (lp=="-1") ? 0 : lp);
			newgu += gu.substring(rp, gu.length);
			parent.opener.document.getElementById("PartysInfo").value = newgu;
		}
	}
	parent.opener.ifrmMemberlist.location.reload(); 
}


// replaceuser
function userChg (pid, pfname, plname, pemail)
{
	parent.opener.document.frmReplaceuser.NewUserID.value = pid;
	parent.opener.document.frmReplaceuser.NewUserFirstName.value = pfname;
	parent.opener.document.frmReplaceuser.NewUserLastName.value = plname;
	parent.opener.document.frmReplaceuser.NewUserEmail.value = pemail;
}


function approverChg(pid, pfname, plname, pemail, cb)
 {
    pfname = replaceSpcChar(pfname.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
    plname = replaceSpcChar(plname.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
	if (queryField("n") == "-1") {
		cbsubname = ".BridgeAdmin"
	} else {
		cbsubname = ".Approver" + queryField("n");
		
		for (i = 0; (i < 3) && (i != queryField("n")); i++) {
			if (pid == eval("parent.opener.document." + queryField("fn") + ".Approver" + i + "ID").value) {
				alert("Error: This user has been seleced as other approver. Please select other approver")
				cb.checked = false;
				return -1;
			}
		}
	}
	eval("parent.opener.document." + queryField("fn") + cbsubname + "ID").value = pid;
	if ( (cb = eval("parent.opener.document." + queryField("fn") + cbsubname + "FN")) != null	)
		cb.value = pfname;		
	if ( (cb = eval("parent.opener.document." + queryField("fn") + cbsubname + "LN")) != null	)
		cb.value = plname;
	eval("parent.opener.document." + queryField("fn") + cbsubname).value = pfname + " " + plname;
}

function approverChgNET(pid, pfname, plname, pemail, internalNum, externalNum, webExConf, dialString, cb) //ZD 100221 //ZD 104289
{
    var parentPage = "ConferenceSetup", WebExEnable = "0";  //ZD 101015
    pfname = replaceSpcChar(pfname.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
    plname = replaceSpcChar(plname.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
    if (parent.opener.document.location.href.indexOf("ExpressConference") > 0) //FB 1779
        parentPage = "ExpressConference";
    //FB 2501 starts
    if (parent.opener.document.location.href.indexOf("ReportDetails") > 0)
        parentPage = "ReportDetails";
    // FB 2501 ends
    //FB 2632 - Starts
    if (parent.opener.document.location.href.indexOf("SearchConferenceInputParameters") > 0) //FB 2632
        parentPage = "SearchConferenceInputParameters";
    //FB 2632 - End
    //ZD 101433
    if (parent.opener.document.location.href.toLowerCase().indexOf("searchuserinputparameters") > -1)
        parentPage = "SearchUserInputParameters";
    
    //ZD 100757- START
    var Imgid = "";
    if (window.parent.location.hash != null)
        Imgid = window.parent.location.hash.split('#')[1];

    if (Imgid != "" && Imgid != null) {
        parent.opener.document.getElementById(Imgid).value = pfname + " " + plname;
        parent.opener.document.getElementById("CATMainGrid_" + Imgid.split('_')[1] + "_lblAssignedAdminID").innerHTML = pid;
        parent.opener.document.getElementById("CATMainGrid_" + Imgid.split('_')[1] + "_hdnApprover1a").value = pid;
        // parent.opener.document.getElementById("CATMainGrid_" + Imgid.split('_')[1] + "_hdnApprover1b").value = pid;        

    }
    //ZD 100757- END
        
     if (parent.opener.document.location.href.indexOf(parentPage) > 0)//FB 1779
    		cbsubname = "hdnApprover" + (parseInt(queryField("n"))+1);
    	else
    		cbsubname = "hdnApprover" + (parseInt(queryField("n"))+1);    		
    	
    	if (parent.opener.document.location.href.indexOf("Inventory") < 0 && parent.opener.document.location.href.indexOf("ConferenceOrder") < 0 && parent.opener.document.location.href.indexOf(parentPage) < 0)//FB 1779
    	    for (i = 0; i < 3; i++) {
    	        if (parent.opener.document.getElementById("hdnApprover" + (i + 1)) != null)//ZD 101525
                {
                    if ((i != queryField("n")) && (pid == parent.opener.document.getElementById("hdnApprover" + (i + 1)).value)) {
                        alert("Error: This user has already been selected as an approver. Please select another approver")
                        cb.checked = false;
                        return -1;
                }
			}
		}

		if (parent.opener.document.getElementById(cbsubname) != null)//ZD 100757
		    parent.opener.document.getElementById(cbsubname).value = pid;
	    if (parent.opener.document.location.href.indexOf(parentPage) > 0)//FB 1779
        {
            parent.opener.document.getElementById("txtApprover" + (parseInt(queryField("n"))+1)).value = pfname + " " + plname;
            //Code added for FB 1116
            //FB 2501 starts
            if ("txtApprover" + (parseInt(queryField("n")) + 1).toString() != "txtApprover7") {
                if (parent.opener.document.getElementById("hdnApproverMail"))
                    parent.opener.document.getElementById("hdnApproverMail").value = pemail;
               
            }
            else {
                if (parent.opener.document.getElementById("hdnRequestorMail"))
                    parent.opener.document.getElementById("hdnRequestorMail").value = pemail;
                
            }
            //FB 2501 starts ends
                
            //FB 2376
            if(parent.opener.document.getElementById("txtintbridge"))
                parent.opener.document.getElementById("txtintbridge").value = internalNum;                
            if(parent.opener.document.getElementById("hdnintbridge"))
                parent.opener.document.getElementById("hdnintbridge").value = internalNum;            
            if(parent.opener.document.getElementById("txtextbridge"))
                parent.opener.document.getElementById("txtextbridge").value = externalNum; 
            if(parent.opener.document.getElementById("hdnextbridge"))
                parent.opener.document.getElementById("hdnextbridge").value = externalNum;
            //FB 2376
            
            //ZD 100221 Starts //ZD 101015 Starts
            if (queryField("t") != null) {

                if (queryField("t") == "1") {
                    if (parent.opener.document.getElementById("hdnHostWebEx"))
                        parent.opener.document.getElementById("hdnHostWebEx").value = webExConf;

                    //ZD 104289
                    if (parent.opener.document.getElementById("hdnDialString"))
                        parent.opener.document.getElementById("hdnDialString").value = dialString;

                    if (parent.opener.document.getElementById("hdnIsHostChanged"))
                        parent.opener.document.getElementById("hdnIsHostChanged").value = "1";
                    
                    if(parent.opener.document.getElementById("lstAudioParty"))
                        parent.opener.document.getElementById("lstAudioParty").click();
                        
                }
                else if (queryField("t") == "2") {
                    if (parent.opener.document.getElementById("hdnSchdWebEx"))
                        parent.opener.document.getElementById("hdnSchdWebEx").value = webExConf;
                }
                

                if (parent.opener.document.getElementById("hdnHostWebEx") != null && parent.opener.document.getElementById("hdnSchdWebEx") != null && parent.opener.document.getElementById("hdnEnableWebExIngt") != null) {
                    if (parent.opener.document.getElementById("hdnHostWebEx").value == "1" && parent.opener.document.getElementById("hdnSchdWebEx").value == "1" && parent.opener.document.getElementById("hdnEnableWebExIngt").value == "1")
                        WebExEnable = "1";

                    if (parent.opener.document.getElementById("trWebex") != null) {
                        if (WebExEnable == '1')
                            parent.opener.document.getElementById("trWebex").style.display = "";
                        else {
                            parent.opener.document.getElementById("trWebex").style.display = "none";
                            parent.opener.document.getElementById("chkWebex").checked = false;
                            parent.opener.document.getElementById("divWebPW").style.display = "none";
                            parent.opener.document.getElementById("divWebCPW").style.display = "none";
                        }
                    }
                }
            }

            //ZD 100221 Ends //ZD 101015 End
            //FB 2426 Start
            if(parent.opener.document.getElementById("txtEmailId"))
                parent.opener.document.getElementById("txtEmailId").value = pemail;
			//ZD 100619 Starts
            if (parent.opener.document.getElementById("hdnApprover5"))
                parent.opener.document.getElementById("hdnApprover5").value = pemail;

            if (parent.opener.document.getElementById("hdnGusetAdmin"))
                parent.opener.document.getElementById("hdnGusetAdmin").value = pid; 
			//ZD 100619 Ends
			//ZD 100574 - Inncrewin Starts 
            if (parent.opener.document.getElementById("txtHost_exp")) {
                parent.opener.document.getElementById("txtHost_exp").value = pfname + " " + plname;
               
            }
            if (parent.opener.document.getElementById("hdntxtHost_exp")) {
                parent.opener.document.getElementById("hdntxtHost_exp").value = pid;
                //parent.opener.saveHostName();
            }
            //ZD 100574 - Inncrewin Ends
            //FB 2426 End
        }    
    	else
    	{
	        if (parent.opener.document.getElementById("txtApprover" + (parseInt(queryField("n"))+1) + "_1") != null)
	        {
	        
	            parent.opener.document.getElementById("txtApprover" + (parseInt(queryField("n"))+1) + "_1").value = pfname + " " + plname;
	            parent.opener.document.getElementById("hdnApprover" + (parseInt(queryField("n"))+1) + "_1").value = pid;
	            parent.opener.document.getElementById("txtApprover" + (parseInt(queryField("n"))+1)).value = pfname + " " + plname;
	            parent.opener.document.getElementById("hdnApprover" + (parseInt(queryField("n"))+1)).value = pid;
	        }
	        else {
	            if (parent.opener.document.getElementById("txtApprover" + (parseInt(queryField("n")) + 1)) != null)//ZD 100757	         
	            parent.opener.document.getElementById("txtApprover" + (parseInt(queryField("n"))+1)).value =  pfname + " " + plname;    
	            
	        }
	    }
}

function replaceuserChg (pid, pfname, plname, pemail, cb)
{
	parent.opener.document.getElementById ("nfn").value = pfname;
	parent.opener.document.getElementById ("nln").value = plname;
	parent.opener.document.getElementById ("ne").value = pemail;
	parent.opener.document.getElementById ("nlgn").value = "";
	parent.opener.document.getElementById ("npwd").value = "";
}


function roomassistChg (pid, pfname, plname, pemail, cb)
{
    //Room Assitant Incharge multi page issue - start
    var fnName ="";
    
    if(queryField("fn") == "")
        fnName = "frmMainroom";
    else
        fnName = queryField("fn");
    pfname = replaceSpcChar(pfname.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
    plname = replaceSpcChar(plname.replace(new RegExp("!!", "g"), "'"),'\"',"||"); //FB 1888
	eval("parent.opener.document." + fnName + ".AssistantID").value = pid;
	eval("parent.opener.document." + fnName + ".AssistantName").value = pfname + " " + plname;
	eval("parent.opener.document." + fnName + ".Assistant").value = pfname + " " + plname;
	if (eval("parent.opener.document." + fnName + ".AssistantEmail") != null)
	    eval("parent.opener.document." + fnName + ".AssistantEmail").value = pemail;  //ZD 100619 //ZD 100522
	if (eval("parent.opener.document." + fnName + ".txtContactEmail") != null)
	    eval("parent.opener.document." + fnName + ".txtContactEmail").value = pemail;  //ZD 100619
	
	//Room Assitant Incharge multi page issue - end
}

function sortlist(id)
{
	SortRows(party_email, id);
}

// user, approver and roomassist do not have reset
function Reset ()
{
	ary = (document.frmEmaillist2.ptyinfos.value).split("??");//FB 1888
	for (i = 0; i < ary.length-1; i++) {
		ary[i] = ary[i].split("~");//FB 1888        
		switch (queryField("frm")) {
			case "party2":	
				cb1 = eval("document.frmEmaillist2.s" + ary[i][0]);
				break;
			//Code Added by Offshore FB issue no:412-Start
			case "party2Aspx":	
				cb1 = eval("document.frmEmaillist2.s" + ary[i][0]);
				break;
			//Code Added by Offshore FB issue no:412-End
			case "party2NET":	
				cb1 = eval("document.frmEmaillist2.s" + ary[i][0]);
				break;
			default:
				cb1 = document.getElementById("0" + ary[i][3]);
				break;
		}
      if(cb1 !=null)//ZD 100393
		if (cb1.checked)	{
			cb1.checked = false;
			
			pi = "";
			for (j = 0; j < ary[i].length; j++) {
				pi += ary[i][j] + "!!"//FB 1888
			}
			pi = pi.substr(0, pi.length-1)
			partyChg (pi);
		}

		//Code Added by Offshore FB issue no:412-Start (ManageTemplate2)
		 if ((queryField("frm") == "party2") || (queryField("frm") == "party2NET") || (queryField("frm") == "party2Aspx")) 
		//Code Added by Offshore FB issue no:412-Ends (ManageTemplate2)
		{
			cb2 = eval("document.frmEmaillist2.i" + ary[i][0]);
			cb3 = eval("document.frmEmaillist2.b" + ary[i][0]);
			cb4 = eval("document.frmEmaillist2.d" + ary[i][0]);
			//Code Modified on 12Mar09 -FB 412- start
			if(cb2)			
			    cb2[0].checked = true;
			if(cb3)	
			    cb3.checked = false;
			if(cb4)	
			    cb4[1].checked = true;
		    //Code Modified on 12Mar09 -FB 412- End
		}
	}
}
//-->
</script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
   <form name="frmEmaillist2" id="frmEmaillist2" runat="server" method="POST">
   <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
<input type="hidden" name="emailListPage" value="<% =Convert.ToString( Convert.ToInt32(Request.QueryString["page"])+1 ) %>"/>
<input name="sortby" id="sortBy" type="hidden"  runat="server" />
<input name="alphabet" id="alphabet" type="hidden"  runat="server" />
<input name="pageNo" id="pageNo" type="hidden"  runat="server" />
<input name="canAll" id="canAll" type="hidden"  runat="server" />
<input name="totalPages" id="totalPages" type="hidden"  runat="server" />
<input type="hidden" name="xmljs" id="xmljs" runat="server" />
<input type="hidden" name="fromSearch" id="fromSearch"  runat="server" />
<input type="hidden" name="FirstName" id="FirstName"  runat="server" />
<input type="hidden" name="LastName" id="LastName"  runat="server" />
<input type="hidden" name="LoginName" id="LoginName"  runat="server" />
<input runat="server" type="hidden" id="HdnAudParty" name="HdnAudParty" /> <%--FB 1779--%>
	

<%
if (Request.QueryString["frm"]!= "") 
	Response.Write ("<input type='hidden' name='frm'  value='" + Request.QueryString["frm"] + "'>");
else
    Response.Write("<input type='hidden' name='frm'  value='" + Request.Form["frm"] + "'>");

if (Request.QueryString["srch"] == "y")
{
    Response.Write("<input type='hidden' name='fromSearch'  value='yes'>");
    Response.Write("<input type='hidden' name='LoginName' value='" + Request.QueryString["gname"] + "'>");
    Response.Write("<input type='hidden' name='FirstName'  value='" + Request.QueryString["fname"] + "'>");
    Response.Write("<input type='hidden' name='LastName'  value='" + Request.QueryString["lname"] + "'>");
}
%>


 <center>
 <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>

<script language="JavaScript">
<!-- 

//	if (navigator.appName == "Netscape")//Commented for FF Fix
//			window.onerror = errorHandler;	

	if ( (!parent.document.frmEmaillist2main.sortby) || (!parent.chgpageoption) || (!parent.document.frmEmaillist2main.fromSearch) || (!parent.tabAllshow) ) {
		setTimeout('window.location.reload();',500);
	}
    
    xmlstr = document.frmEmaillist2.xmljs.value;    
    
	parent.tabAllshow(parseInt(document.frmEmaillist2.canAll.value, 10));   
	
	parent.chgpageoption (document.frmEmaillist2.totalPages.value);	
	 
	 if (parseInt(document.frmEmaillist2.totalPages.value, 10) > 0)
		    parent.pageimg (document.frmEmaillist2.pageNo.value)   
		    
  	if (parent.document.frmEmaillist2main.fromSearch.value != "y") {		
		
		parent.nameimg ("", document.frmEmaillist2.alphabet.value)
	}	

	var party_email = new SortTable('party_email');
	
	switch (queryField("frm")) {
		case "party2":
			if (isFuCreate || isRm) {
				wid = 12;
			} else {
				wid = 10;
			}
			party_email.AddColumn("First Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Last Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Login Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Email","style='width: " + (wid+8) + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Selected","style='width: " + (wid-2) + "%; font-size: 10px;' class='tableHeader'","center","form");
				
			if (isFuCreate || isRm) {
				party_email.AddColumn("Invite","width=11%; font-size: 10px; class='tableHeader'","center","form");
			} else {
				party_email.AddColumn("Invited","width=10%; font-size: 10px; class='tableHeader'","center","form");
				party_email.AddColumn("Invitee","width=10%; font-size: 10px; class='tableHeader'","center","form");
			}
			party_email.AddColumn("CC","width=3%; font-size: 10px; class='tableHeader'","center","form");
			party_email.AddColumn("Notify","width=8%; font-size: 10px; class='tableHeader'","center","form");
			party_email.AddColumn("Audio","width=8%; font-size: 10px; class='tableHeader'","center","form");
			party_email.AddColumn("Video","width=8%; font-size: 10px; class='tableHeader'","center","form");
		    
			break;
		//Code Added by Offshore FB issue no:412-Start (ManageTemplate2)
		case "party2Aspx":
			if (isFuCreate || isRm) {
				wid = 12;
			} else {
				wid = 10;
			}
			party_email.AddColumn("First Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Last Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Login Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Email","style='width: " + (wid+8) + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Selected","style='width: " + (wid-2) + "%; font-size: 10px;' class='tableHeader'","center","form");
			//alert(isFuCreate || isRm);
			//Code Commented For myVrmLookUP From templates on 20Mar09- FB 412 - Start
			//if (isFuCreate || isRm) {
			//party_email.AddColumn("Invite","width=11%; font-size: 10px; class='tableHeader'","center","form");
			//} else {
			//	party_email.AddColumn("Invited","width=10%; font-size: 10px; class='tableHeader'","center","form");
			//	party_email.AddColumn("Invitee","width=10%; font-size: 10px; class='tableHeader'","center","form");
			//}
			//party_email.AddColumn("CC","width=3%; font-size: 10px; class='tableHeader'","center","form");
			//party_email.AddColumn("Notify","width=8%; font-size: 10px; class='tableHeader'","center","form");
			//party_email.AddColumn("Audio","width=8%; font-size: 10px; class='tableHeader'","center","form");
			//party_email.AddColumn("Video","width=8%; font-size: 10px; class='tableHeader'","center","form");
			//Code Commented For myVrmLookUP From templates on 20Mar09- FB 412 - End
			break;
		//Code Added by Offshore FB issue no:412-End (ManageTemplate2)
		case "party2NET":
			if (isFuCreate || isRm) {
				wid = 12;
			} else {
				wid = 10;
			}
			party_email.AddColumn("First Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Last Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Login Name","style='width: " + wid + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Email","style='width: " + (wid+8) + "%; font-size: 10px;' class='tableHeader'","center","email");
			party_email.AddColumn("Selected","style='width: " + (wid-2) + "%; font-size: 10px;' class='tableHeader'","center","form");
			break;
		default:
			party_email.AddColumn("First Name"," class='tableHeader'","center","email");
			party_email.AddColumn("Last Name"," class='tableHeader'","center","email");
			party_email.AddColumn("Login Name"," class='tableHeader'","center","email");
			party_email.AddColumn("Email"," class='tableHeader'","center","email");
			party_email.AddColumn("Selected"," class='tableHeader'","center","form");
			break;
	}
	usersary = xmlstr.split("==");	//FB 1888D
	j = -1;
	pinfos = ""

	for (i = 0; i < usersary.length-1; i++) {
		usersary[i] = usersary[i].split("%%")
   
		comp = "";
		switch ( queryField("st") ) {
			case "0":	//l
				comp = ( (usersary[i][2]).substr(0,1) ).toUpperCase()
				break;
			case "1":	//f 
				comp = ( (usersary[i][1]).substr(0,1) ).toUpperCase()
				break;
			case "2":	//e
				comp = ( (usersary[i][3]).substr(0,1) ).toUpperCase()
				break;
			case "3":	//g
				comp = ( (usersary[i][4]).substr(0,1) ).toUpperCase()
				break;
		}

		if ( (queryField("sn") == "0") || (queryField("sn") == comp) ) {
			j++;

                    //Code added for FB 1116
		            var mail = usersary[i][3]; 
		            var name = usersary[i][2];
		            var fname = usersary[i][1];
		            var survey = usersary[i][5]; //FB 2348
		             //Commented for lookup changes start
					/*if (usersary[i][0].length > 7)
						usersary[i][0] = usersary[i][0].substr(0,7) + "..."
					if (usersary[i][1].length > 7)
						usersary[i][1] = usersary[i][1].substr(0,7) + "..."
					if (usersary[i][2].length > 7)
						usersary[i][2] = usersary[i][2].substr(0,7) + "..."
					if (usersary[i][3].length > 25)
						usersary[i][3] = usersary[i][3].substr(0,25) + "..."*/
					//Commented for lookup changes End
			switch (queryField("frm")) {
				case "party2":	
					pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3] + ",1," + j;
					selectstr = "<input type='checkbox' name='s" + usersary[i][0] + "' value='1' " + "onclick='JavaScript: partyChg(\"" + pinfostr + "\");'>";

					tmpstr1 = "onclick='JavaScript:partyChg(\"" + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3] + ",0," + j + "\");'"
					tmpstr2 = "class='normalFormat'"
					tmpstr = " " + tmpstr1 + " " + tmpstr2 + ">"
					invitedstr = "<input type='radio' name='i" + usersary[i][0] + "' value='1' checked" + tmpstr
					inviteestr = "<input type='radio' name='i" + usersary[i][0] + "' value='2'" + tmpstr

					if ("<%=Application["Client"]%>".toUpperCase() == "SCTECH"){
						ccstr = "<input type='radio' name='i" + usersary[i][0] + "' value='0' checked" + tmpstr
						notifystr = "<input type='checkbox' name='b" + usersary[i][0] + "' value='1' checked" + tmpstr
					}
					else{
						ccstr = "<input type='radio' name='i" + usersary[i][0] + "' value='0'" + tmpstr
						notifystr = "<input type='checkbox' name='b" + usersary[i][0] + "' value='1' checked" + tmpstr

					}

					audiostr = "<input type='radio' name='d" + usersary[i][0] + "' value='1'" + tmpstr
					videostr = "<input type='radio' name='d" + usersary[i][0] + "' value='2' checked" + tmpstr

					break;
				//Code Added by Offshore FB issue no:412-Start (ManageTemplate2)
				case "party2Aspx":
					pinfostr = usersary[i][0] + "~" + replaceSpcChar((usersary[i][1].replace(new RegExp("\"","g"),"||")), "!!","'") + "~" + replaceSpcChar((usersary[i][2].replace(new RegExp("\"","g"),"||")), "!!","'") + "~" + usersary[i][3] + "~1~" + j; //FB 1888
					selectstr = "<input type='checkbox' name='s" + usersary[i][0] + "' value='1' " + "onclick='JavaScript: partyChg(\"" + pinfostr + "\");'>";
                    //Code Commented For myVrmLookUP From templates on 20Mar09- FB 412 - Start	
					//tmpstr1 = "onclick='JavaScript:partyChg(\"" + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3] + ",0," + j + "\");'"
					//tmpstr2 = "class='normalFormat'"
					//tmpstr = " " + tmpstr1 + " " + tmpstr2 + ">"
					//invitedstr = "<input type='radio' name='i" + usersary[i][0] + "' value='1' checked" + tmpstr
					//inviteestr = "<input type='radio' name='i" + usersary[i][0] + "' value='2'" + tmpstr

					//if ("<%=Application["Client"]%>".toUpperCase() == "SCTECH"){
					//	ccstr = "<input type='radio' name='i" + usersary[i][0] + "' value='0' checked" + tmpstr
					//	notifystr = "<input type='checkbox' name='b" + usersary[i][0] + "' value='1' checked" + tmpstr
					//}
					//else{
					//	ccstr = "<input type='radio' name='i" + usersary[i][0] + "' value='0'" + tmpstr
					//	notifystr = "<input type='checkbox' name='b" + usersary[i][0] + "' value='1' checked" + tmpstr

					//}

					//audiostr = "<input type='radio' name='d" + usersary[i][0] + "' value='1'" + tmpstr
					//videostr = "<input type='radio' name='d" + usersary[i][0] + "' value='2' checked" + tmpstr
                    //Code Commented For myVrmLookUP From templates on 20Mar09- FB 412 - End
					break;
				//Code Added by Offshore FB issue no:412-End (ManageTemplate2)
				case "party2NET": //ConferenceSetup
				    //pinfostr = usersary[i][0] + "~" + usersary[i][1] + "~" + usersary[i][2] + "~" + usersary[i][3].toLowerCase() + "~1~" + j;//FB 1888
				    pinfostr = usersary[i][0] + "~" + replaceSpcChar((usersary[i][1].replace(new RegExp("\"","g"),"||")), "!!","'") + "~" + replaceSpcChar((usersary[i][2].replace(new RegExp("\"","g"),"||")), "!!","'") + "~" + usersary[i][3].toLowerCase() + "~1~" + usersary[i][5] + "~" + j;//FB 1888 //FB 2348
					selectstr = "<input type='checkbox' name='s" + usersary[i][0] + "' value='1' " + "onclick='JavaScript: partyChg(\"" + pinfostr + "\");'>";

					tmpstr1 = "onclick='JavaScript:partyChg(\"" + usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" +  usersary[i][3].toLowerCase() + "!!0!!" + usersary[i][5] + "!!" + j + "\");'" //FB 1888 //FB 2348
					tmpstr2 = "class='normalFormat'"
					tmpstr = " " + tmpstr1 + " " + tmpstr2 + ">"
					break;
				case "users":	
                    // rplc us
					//pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3];
					pinfostr = usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" + usersary[i][3];//FB 1888

					selectstr = "<input type='radio' name='newuser' value='1' " + 
						"onclick='JavaScript: userChg(\"" + usersary[i][0] + "\", \"" + usersary[i][1] + "\", \"" + usersary[i][2] + "\", \"" + usersary[i][3] + "\");'>"
					break;
				case "approver":	// approver in rm, suad, bs
					//pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3];
					pinfostr = usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" + usersary[i][3];//FB 1888

					selectstr = "<input type='radio' name='newuser' value='1' " + 
						"onclick='JavaScript: approverChg(\"" + usersary[i][0] + "\", \"" +  replaceSpcChar((usersary[i][1].replace(new RegExp("\"","g"),"||")), "!!","'") + "\", \"" +  replaceSpcChar((usersary[i][2].replace(new RegExp("\"","g"),"||")), "!!","'") + "\", \"" + usersary[i][3] + "\", this);'>"
					break;
				case "replaceuser":
					//pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3];
					pinfostr = usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" + usersary[i][3];//FB 1888

					selectstr = "<input type='radio' name='newuser' value='1' " + 
						"onclick='JavaScript: replaceuserChg(\"" + usersary[i][0] + "\", \"" + usersary[i][1] + "\", \"" + usersary[i][2] + "\", \"" + usersary[i][3] + "\", this);'>"
					break;
				case "roomassist":	// assist in rm
					//pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3];
					pinfostr = usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" + usersary[i][3];//FB 1888

					selectstr = "<input type='radio' name='newuser' value='1' " + 
						"onclick='JavaScript: roomassistChg(\"" + usersary[i][0] + "\", \"" +  replaceSpcChar((usersary[i][1].replace(new RegExp("\"","g"),"||")), "!!","'") + "\", \"" +  replaceSpcChar((usersary[i][2].replace(new RegExp("\"","g"),"||")), "!!","'") + "\", \"" + usersary[i][3] + "\", this);'>"
					break;
				case "approverNET": // from SuperAdministrator.aspx page, change SYSTEM approver(s)
					//pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3]; FB 1888
					pinfostr = usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" + usersary[i][3];
                    fname = replaceSpcChar((usersary[i][1].replace(new RegExp("\"","g"),"||")), "!!","'"); // FB 1888
                    name = replaceSpcChar((usersary[i][2].replace(new RegExp("\"","g"),"||")), "!!","'"); // FB 1888
                    
					selectstr = "<input type='radio' name='newuser' value='1' onclick='JavaScript: approverChgNET(\"" + usersary[i][0] + "\", \"" + fname + "\", \"" + name + "\", \"" + mail + "\", \"" + usersary[i][6] + "\", \"" + usersary[i][7] + "\", \"" + usersary[i][8] + "\", \"" + usersary[i][9] + "\" , this);'>" //FB 2376 //ZD 100221 //ZD 104289
					  //Code added & COmmented for FB 1116 //"onclick='JavaScript: approverChgNET(\"" + usersary[i][0] + "\", \"" + usersary[i][1] + "\", \"" + usersary[i][2] + "\", \"" + usersary[i][3] + "\", this);'>"
						break;
				default:		// gp
									//pinfostr = usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3] + ",1," + j;
					pinfostr = usersary[i][0] + "!!" + usersary[i][1] + "!!" + usersary[i][2] + "!!" + usersary[i][3] + "!!1!!" + j;//FB 1888
                
					selectstr = "<input type='checkbox' name='0" + usersary[i][3] + "' id='0" + usersary[i][3] + "' value='1' " + 
								//"onclick='JavaScript: memberChg(\"" + usersary[i][0] + "," + usersary[i][1] + "," + usersary[i][2] + "," + usersary[i][3] + ",-5;\", \"" + usersary[i][3] + "\");'>" // FB 1888
								"onclick='JavaScript: memberChg(\"" + usersary[i][0] + "!!" + replaceSpcChar((usersary[i][1].replace(new RegExp("!!","g"),"~")), "?","||") + "!!" + replaceSpcChar((usersary[i][2].replace(new RegExp("!!","g"), "~")), "?","||") + "!!" + usersary[i][3] + "!!-5||\", \"" + usersary[i][3] + "\");'>" // FB 1888
					break;
			}
			
			namestr = usersary[i][1] + " " + usersary[i][2]
			
			if(usersary[i][3].length > 18){
				strLongEmail = usersary[i][3].substr(0,17) + "..."
			}
			else{
				strLongEmail = usersary[i][3]
			}
			emailstr = "<a href='mailto:" + usersary[i][3] + "' title='" + usersary[i][3] + "'>" + strLongEmail + "</a>"
            //FB 1888 start
            usersary[i][1] = replaceSpcChar(usersary[i][1].replace(new RegExp("!!","g"), "'"),'\"',"||");
            usersary[i][2] = replaceSpcChar(usersary[i][2].replace(new RegExp("!!","g"), "'"),'\"',"||"); //FB 1888
            //FB 1888 
			switch (queryField("frm")) {
				case "party2":
					
					if (isFuCreate || isRm)
						party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr, invitedstr, ccstr, notifystr, audiostr, videostr);
					else
						party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr, invitedstr, inviteestr, ccstr, notifystr, audiostr, videostr);
					break;
				//Code Added by Offshore FB issue no:412-Start (ManageTemplate2)
			    case "party2Aspx":
			    //Code Added by Offshore FB issue no:412-Start (ManageTemplate2.aspx)
					// selectstr,invitedstr,inviteestr,ccstr,notifystr,audiostr,videoeestr,namestr,emailstr
					//if (isFuCreate || isRm)
					//	party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr, invitedstr, ccstr, notifystr, audiostr, videostr);
					//else
					//	party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr, invitedstr, inviteestr, ccstr, notifystr, audiostr, videostr);						
				    party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr);
			    //Code Modified For myVrmLookUP From templates on 20Mar09- FB 412 - End 
					break;
				//Code Added by Offshore FB issue no:412-End (ManageTemplate2)
				case "party2NET":
					party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr);
					break;
				default:
					party_email.AddLine(usersary[i][1], usersary[i][2], usersary[i][4], emailstr, selectstr);
					break;
			}
			pinfos += pinfostr + "??"//FB 1888
		}
	}

//-->
</script>
<div id="container">
<%--Window Dressing--%>
    <table class="tablebody">

	<script type="text/javascript">
	
		document.write(parent.document.frmEmaillist2main.mt.value);
    
//Window Dressing
		if (parseInt(document.frmEmaillist2.totalPages.value, 10) == 0) {
		    document.write("<tr><td colspan='10' class='tableBody'><font class='lblError'><asp:Literal Text='<%$ Resources:WebResources, NoUsers%>' runat='server'></asp:Literal></font></td></tr>")
    }
    else
        party_email.WriteRows()
	</script>

    </table>
</div>
  </center>

  <input type="hidden" id="ptyinfos" value=""/>
</form>

<script type="text/javascript" language="javascript"> 

	document.getElementById("ptyinfos").value = pinfos;
</script>
</body>
</html>
