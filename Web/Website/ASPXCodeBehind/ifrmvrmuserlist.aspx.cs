/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class en_IfrmVRMUserlist : System.Web.UI.Page
{
    myVRMNet.NETFunctions obj;
    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (obj == null)
            obj = new myVRMNet.NETFunctions();
        obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower()); // ZD 100263



    }
}
