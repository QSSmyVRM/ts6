//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Globalization;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter.Schemas
{
	public sealed class ElementType : SchemaType, IWriter
	{
		// Fields
		private AttributeCollection _attributes;
		private SchemaContent _content;
		private string _name;

		// Methods
		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("s", "ElementType", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882");
			if (this._name != null)
			{
				writer.WriteAttributeString("s", "name", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._name);
			}
			if (this._content != SchemaContent.NotSet)
			{
				writer.WriteAttributeString("s", "content", "uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882", this._content.ToString(CultureInfo.InvariantCulture));
			}
			if (this._attributes != null)
			{
				((IWriter) this._attributes).WriteXml(writer);
			}
			writer.WriteEndElement();
		}

		// Properties
		public AttributeCollection Attributes
		{
			get
			{
				if (this._attributes == null)
				{
					this._attributes = new AttributeCollection();
				}
				return this._attributes;
			}
		}

		public SchemaContent Content
		{
			get
			{
				return this._content;
			}
			set
			{
				this._content = value;
			}
		}

		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}
	}


}
