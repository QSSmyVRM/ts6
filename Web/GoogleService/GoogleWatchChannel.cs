﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Web;
using Google.Apis.Util;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Requests;
using Google.Apis.Services;
using System.Threading.Tasks;
using System.Net;

namespace GoogleService
{
   
    class GoogleWatchChannel
    {

        

        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static ASPIL.VRMServer myvrmCom = null;
        static VRMRTC.VRMRTC objRTC = null;
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        static System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);


        XmlDocument _Xdoc = null;
        string _OutXML = "", _ClientID = "", _SecretID = "", _ChannelAddrss = "", _RefreshToken = "", _UserEmail = "", _UserID = "";
        StringBuilder _inXML = null;


        NS_LOGGER.Log log = null;
        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        static bool ret = false;
      



        internal GoogleWatchChannel()
        {
            myvrmCom = new ASPIL.VRMServer();
            config = new NS_CONFIG.Config();
            configParams = new NS_MESSENGER.ConfigParams();
            ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath,RTC_ConfigPath); //FB 2944
            log = new NS_LOGGER.Log(configParams);
            
        }

        internal void GoogleChannelupdate()
        {
            try
            {
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();

                _inXML = new StringBuilder();
                _OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetGoogleChannelExpiredList", _inXML.ToString());
                log.Trace("OutXML: GetGoogleChannelExpiredList:" + _OutXML);

                if (_OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in GetGoogleChannelExpiredList");
                else
                {
                    _Xdoc = new XmlDocument();
                    _Xdoc.LoadXml(_OutXML);

                    _ClientID = "";
                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleClientID") != null)
                        _ClientID = _Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleClientID").InnerText;

                    _SecretID = "";
                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleSecretID") != null)
                        _SecretID = _Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/GoogleSecretID").InnerText;

                    _ChannelAddrss = "";
                    if (_Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/ChannelAddress") != null)
                        _ChannelAddrss = _Xdoc.SelectSingleNode("//GetGoogleChannelExpiredList/ChannelAddress").InnerText;



                    XmlNodeList _NodeList = _Xdoc.GetElementsByTagName("User");
                    XmlElement _Element = null;

                    for (int _i = 0; _i < _NodeList.Count; _i++)
                    {
                        _Element = (XmlElement)_NodeList[_i];
                        _RefreshToken = ""; _UserEmail = ""; _UserID = "";
                        _RefreshToken = _Element.GetElementsByTagName("RefreshToken")[0].InnerText;
                        _UserID = _Element.GetElementsByTagName("UserID")[0].InnerText;
                        _UserEmail = _Element.GetElementsByTagName("UserEmail")[0].InnerText;

                        GetAuthentication();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }

        #region GetAuthentication
        /// <summary>
        /// GetAuthentication
        /// </summary>
        private void GetAuthentication()
        {
            Google.Apis.Calendar.v3.Data.Channel _Channel = null;
            Google.Apis.Calendar.v3.Data.Channel _ChannelRes = null;
            try
            {
                var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description)
                {
                    ClientIdentifier = _ClientID,
                    ClientSecret = _SecretID
                };
                var auth = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthorization);

                auth.LoadAccessToken();
                // Create the service.
                var _service = new CalendarService(new BaseClientService.Initializer()
                {
                    Authenticator = auth,
                    ApplicationName = "myVRM Google Calendar Intergration",

                });

                _Channel = new Google.Apis.Calendar.v3.Data.Channel();
                _Channel.Id = Guid.NewGuid().ToString();

                _Channel.Type = "web_hook";
                _Channel.Address = _ChannelAddrss+@"\en\GoogleReceiver.aspx";
                _Channel.Token = _UserID;


                _ChannelRes = new Google.Apis.Calendar.v3.Data.Channel();
                _ChannelRes = _service.Events.Watch(_Channel, _UserEmail).Execute();

                if (_ChannelRes == null)
                    log.Trace("Channel response failed");
                else
                {
                    _inXML = new StringBuilder();
                    _inXML.Append("<UserToken>");
                    _inXML.Append("<UserId>" + _UserID + "</UserId>");
                    _inXML.Append("<ChannelID>" + _ChannelRes.Id + "</ChannelID>");
                    _inXML.Append("<ChannelEtag>" + _ChannelRes.ETag + "</ChannelEtag>");

                    double _Expiration = 0;
                    double.TryParse(_ChannelRes.Expiration, out _Expiration);

                    DateTime _dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    _dtDateTime = _dtDateTime.AddMilliseconds(_Expiration).ToUniversalTime();

                    _inXML.Append("<ChannelExpiration>" + _dtDateTime.ToString() + "</ChannelExpiration>");
                    _inXML.Append("<ChannelResourceId>" + _ChannelRes.ResourceId + "</ChannelResourceId>");
                    _inXML.Append("<ChannelResourceUri>" + _ChannelRes.ResourceUri + "</ChannelResourceUri>");
                    _inXML.Append("</UserToken>");

                    if (myvrmCom == null)
                        myvrmCom = new ASPIL.VRMServer();
                    _OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SetUserToken", _inXML.ToString());

                    if (_OutXML.IndexOf("<error>") >= 0)
                        log.Trace("Error in SetUserToken");
                }

            }
            catch (System.Threading.ThreadAbortException ex)
            {
                log.Trace("Thread abort GetAuthentication Block:" + ex.Message);
            }
            catch (System.Exception exp)
            {
                log.Trace("GetAuthentication Block:" + exp.Message);
            }
        }
        #endregion

        #region GetAuthorization
        /// <summary>
        /// GetAuthorization
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private IAuthorizationState GetAuthorization(NativeApplicationClient arg)
        {
            try
            {
                // Get the auth URL:
                IAuthorizationState state = new AuthorizationState(new[] { CalendarService.Scopes.Calendar.GetStringValue() });
                state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
                Uri authUri = arg.RequestUserAuthorization(state);

                state.RefreshToken = _RefreshToken;
                var tokenRefreshed = arg.RefreshToken(state);
                if (tokenRefreshed)
                    return state;
                else
                    throw new ApplicationException("Unable to refresh the token.");

            }
            catch (WebException wex)
            {
                string error = "";
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {

                            log.Trace(error = reader.ReadToEnd());
                        }
                    }
                }


                return null;
            }
            catch (Exception ex)
            {
                log.Trace("Unable to refresh the token:" + ex.Message);
                return null;

            }


        }
        #endregion
    }
}
